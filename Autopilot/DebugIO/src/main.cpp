#include <iostream>
#include <fstream>
#include <string>
#include <cstdint>

using namespace std;

int main(int argc, char **argv)
{
    int32_t value;
    ifstream instream("data.txt");
    if(! instream.is_open()) {
        cerr << "Failed to open input stream." << endl;
        return 1;
    }
    while(! instream.eof()) {
        instream >> value;
        const unsigned sequence = static_cast<unsigned>(0xFF & (value >> 24));
        // cout << sequence << endl;
        // TBD: Sequence number check.
        for(unsigned i = 1; i < 4; ++i) {
            const unsigned shift = 24 - (8 * i);
            const char c = static_cast<char>(0xFF & (value >> shift));
            if('\0' != c) {
                cout << c;
            }
        }
    }
    cout << endl;
    instream.close();
    cout << "Done." << endl;
	return 0;
}
