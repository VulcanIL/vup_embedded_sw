This archive contains the latest P&E ColdFire V2/V3/V4 interface DLL and firmware for all compatible P&E interfaces. 
If you are unable to utilize your P&E interface with your software (such as Codewarrior, IAR, Greenhills, etc.), 
you may need to apply this update.

Note: The current version of unit_cfz.dll is 5.84.00.00 (released 03/14/2016)

Note: The following files are firmware for P&E interfaces and should reside in the same directory as the DLL. 
The firmware files are required to ensure that your hardware is compatible with the version of the DLL that is included here.

Cyclone Max               - cyclonemaxen2.792
			                cyclonemaxens.750
			                maxkernel2.001
			                maxkernel2.002
Cyclone Universal         - CycloneLCunivAppens.0972
						    CycloneLCunivBootSens.0972
						    CycloneLCunivBootPens.0972
Cyclone Universal FX      - CycloneFXunivAppens.0972
						    CycloneFXunivBootSens.0972
						    CycloneFXunivBootPens.0972
USB-ML-CFE                - usbmlcf20_ens.554
USB-ML-CFE Reference      - usbmlcf20_ref_ens.554
USB-ML-UNIVERSAL Rev A/B  - usbmlfs20cfens.615
USB-ML-UNIVERSAL Rev C    - usbmlfsufcfens.0960
							usbmlfsufbootens.0940
USB-ML-UNIVERSAL-FX Rev A - usbmlfx20cfens.706
USB-ML-UNIVERSAL-FX Rev B - usbmlfxufcfens.0960 
							usbmlfxufbootens.0940


Update Instructions:

Note for IAR Embedded Workbench users: The DLL file is unit_cfz_v234.dll not unit_cfz.dll. Rename the DLL file to fit the same naming convention.

1. Search the application directory for the DLL file (unit_cfz.dll).

2. When the file is located, rename it to unit_cfz.dll.backup or archive it so you can restore it at a later time if necessary.

3. Extract all files from this archive to the directory.

4. Search your C:\windows\system32 directory for the DLL file. If the DLL is there, also replace it with the latest version.  

5. Restart the application debugger and try to debug your target board.

6. If the application is still unable to detect the P&E interface, install the latest P&E drivers from www.pemicro.com


P&E Microcomputer Systems, Inc. All rights reserved.
Visit us at http://www.pemicro.com