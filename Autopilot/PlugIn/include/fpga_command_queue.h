#ifndef FPGA_COMMAND_QUEUE_H_
#define FPGA_COMMAND_QUEUE_H_

#include "interfaces.h" /* For uint32_t. */

enum {
    kFPGACommandQueueCapacity = 32
};

/**
 * @brief 
 * @param entry
 * @return Zero on success, nonzero on failure.
 */
int fpga_command_queue_put(const struct fpga_queue_entry_t entry);

/**
 * @brief 
 * @param entry
 * @param gotEntry
 * @return Zero on success (whether or not an entry was fetched), nonzero if
 * an error occurred.
 */
int fpga_command_queue_get(struct fpga_queue_entry_t* const entry, bool* const gotEntry);

/**
 * @brief Empties the queue.
 */
void fpga_command_queue_clear();

unsigned fpga_command_queue_size();

#endif /* #include guard. */
