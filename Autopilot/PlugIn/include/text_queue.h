#ifndef TEXT_QUEUE_H_
#define TEXT_QUEUE_H_

enum { kTextQueueCapacity = 128 };

void textQueuePutChar(const char c);

void textQueuePutStr(const char* str, const unsigned len);

/**
 * @brief Fetches a single char from the text queue, if one is available.
 * @param charBuffer Buffer space.
 * @param charsFetched Will be populated with the number of chars fetched (0 or 1).
 */
void textQueueGet(char* charBuffer, unsigned* const charsFetched);

#endif /* #include guard */
