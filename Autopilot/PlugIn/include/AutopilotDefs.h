#ifndef AUTOPILOT_DEFS_H
#define AUTOPILOT_DEFS_H
/****************************************************************************

mp2128_example.h

Project  :  MP2128g Coldfire User Code 

Copyright (c) 2006  MicroPilot

Author   :  Vitaliy Degtyaryov 

Synopsis :  Routines for initialization and operation of MP2128 User Code
            example

CVS-TAG  : $ $

****************************************************************************/

/****************************************************************************/
/*  PUBLIC FUNCTION PROTOTYPES                                              */
/****************************************************************************/



/****************************************************************************/
/*  REGISTERS                                                               */
/****************************************************************************/


/*= Frequency Modulated Phase Locked Loop =*/
#define MP2128UC_FMPLL_SYNCR          (*(volatile uint32_t*)(void*)(&__IPSBAR[0x120000]))
#define MP2128UC_FMPLL_SYNSR          (*(volatile uint32_t*)(void*)(&__IPSBAR[0x120004]))

/*= Interrupt Controllers =*/
#define MP2128UC_INTC0_ICRn(x)        (*(volatile unsigned char*)(void*)(&__IPSBAR[0x000C40+((x)*0x001)]))
#define MP2128UC_INTC0_IMRH           (*(volatile uint32_t*)(void*)(&__IPSBAR[0x000C08]))
#define MP2128UC_INTC0_IMRL           (*(volatile uint32_t*)(void*)(&__IPSBAR[0x000C0C]))

#define MP2128UC_INTC1_ICRn(x)        (*(volatile unsigned char*)(void*)(&__IPSBAR[0x000D40+((x)*0x001)]))
#define MP2128UC_INTC1_IMRH           (*(volatile uint32_t*)(void*)(&__IPSBAR[0x000D08]))
#define MP2128UC_INTC1_IMRL           (*(volatile uint32_t*)(void*)(&__IPSBAR[0x000D0C]))

/****************************************************************************/
/*  CONSTANTS                                                               */
/****************************************************************************/
/* ATTENTION! The user shall use these Interrupt Request (IRQ) level and 
priority only! The other IRQ levels and priorities may disrupt the autopilot
code. If the user needs some other level/priority, contact MicroPilot.
See the XTENDER Programming Guide 'Interrupts (mp2128 only)' section for more info
Each interrupt source have a unique interrupt level and priority to ensure proper operation. */

/* Interrupt Controller     Level   Priority    When can it be used?
            0               5       7           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       6           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       5           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       4           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       3           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       2           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       1           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               5       0           can be used unless a CAN protocol is enabled in the autopilot VRS

            0               4       7           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       6           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       5           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       4           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       3           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       2           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       1           can be used unless a CAN protocol is enabled in the autopilot VRS
            0               4       0           can be used unless a CAN protocol is enabled in the autopilot VRS

            0               3       7           can be used anytime
            0               3       6           can be used anytime
            0               3       4           can be used anytime
            0               3       3           can be used anytime
            0               3       2           can be used anytime
            0               3       1           can be used anytime
            0               3       0           can be used anytime

            1               5       7           can be used if TPU 13 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               5       6           can be used anytime
            1               5       5           can be used anytime
            1               5       4           can be used anytime
            1               5       3           can be used if TPU 22 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               5       2           can be used if TPU 20 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               5       1           can be used if TPU 21 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               5       0           can be used if TPU 4 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS

            1               4       7           can be used if TPU 9 (RC Aileron) is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       6           can be used if TPU 0 (AGL input) is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       5           can be used if TPU 14 (RC CH5) is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       4           can be used if TPU 8 (RC Throttle) is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       3           can be used if TPU 12 (RC Rudder) is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       2           can be used if TPU 11 (RC Elevator) is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       1           can be used if TPU 15 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               4       0           can be used if TPU 24 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS

            1               3       7           can be used if TPU 28 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               3       6           can be used if TPU 29 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               3       5           can be used if TPU 30 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               3       4           can be used if TPU 31 is disabled in the hardwareDisable field and not enabled for a custom function by the VRS
            1               3       3           can be used anytime
            1               3       2           can be used anytime
            1               3       1           can be used anytime
            1               3       0           can be used anytime
*/

#define MP2128UC_IRQ_LEVEL_5                5
#define MP2128UC_IRQ_LEVEL_4                4
#define MP2128UC_IRQ_LEVEL_3                3

#define MP2128UC_IRQ_PRIORITY_7             7
#define MP2128UC_IRQ_PRIORITY_6             6
#define MP2128UC_IRQ_PRIORITY_5             5
#define MP2128UC_IRQ_PRIORITY_4             4
#define MP2128UC_IRQ_PRIORITY_3             3
#define MP2128UC_IRQ_PRIORITY_2             2
#define MP2128UC_IRQ_PRIORITY_1             1
#define MP2128UC_IRQ_PRIORITY_0             0

#define MP2128UC_INTC_ENABLE                0xFFFFFFFE
#define MP2128UC_INTC_ICRn_IP(x)            (((x)&0x07)<<0)
#define MP2128UC_INTC_ICRn_IL(x)            (((x)&0x07)<<3)

#define MP2128UC_ETPU_IRQ_CONTROLLER        1



/* eTPU IRQ Source */
/* User-defined IRQ Source can be Assigned according to the 
"MCF5235 Reference Manual",  MCF5235RM, Rev.1.1 09/2004:
Chapter 13.2.1.6.1: Interrupt Sources */
#define MP2128UC_IRQ_SOURCE_ETPU_0            27            
#define MP2128UC_IRQ_SOURCE_ETPU_1            28
#define MP2128UC_IRQ_SOURCE_ETPU_2            29
#define MP2128UC_IRQ_SOURCE_ETPU_3            30
#define MP2128UC_IRQ_SOURCE_ETPU_4            31
#define MP2128UC_IRQ_SOURCE_ETPU_5            32
#define MP2128UC_IRQ_SOURCE_ETPU_6            33
#define MP2128UC_IRQ_SOURCE_ETPU_7            34
#define MP2128UC_IRQ_SOURCE_ETPU_8            35
#define MP2128UC_IRQ_SOURCE_ETPU_9            36
#define MP2128UC_IRQ_SOURCE_ETPU_10           37 
#define MP2128UC_IRQ_SOURCE_ETPU_11           38 
#define MP2128UC_IRQ_SOURCE_ETPU_12           39 
#define MP2128UC_IRQ_SOURCE_ETPU_13           40
#define MP2128UC_IRQ_SOURCE_ETPU_14           41 
#define MP2128UC_IRQ_SOURCE_ETPU_15           42 
#define MP2128UC_IRQ_SOURCE_ETPU_16           43 
#define MP2128UC_IRQ_SOURCE_ETPU_17           44 
#define MP2128UC_IRQ_SOURCE_ETPU_18           45 
#define MP2128UC_IRQ_SOURCE_ETPU_19           46 
#define MP2128UC_IRQ_SOURCE_ETPU_20           47 
#define MP2128UC_IRQ_SOURCE_ETPU_21           48 
#define MP2128UC_IRQ_SOURCE_ETPU_22           49
#define MP2128UC_IRQ_SOURCE_ETPU_23           50 
#define MP2128UC_IRQ_SOURCE_ETPU_24           51 
#define MP2128UC_IRQ_SOURCE_ETPU_25           52 
#define MP2128UC_IRQ_SOURCE_ETPU_26           53 
#define MP2128UC_IRQ_SOURCE_ETPU_27           54 
#define MP2128UC_IRQ_SOURCE_ETPU_28           55 
#define MP2128UC_IRQ_SOURCE_ETPU_29           56 
#define MP2128UC_IRQ_SOURCE_ETPU_30           57 
#define MP2128UC_IRQ_SOURCE_ETPU_31           58 

/*  IRQ Controllers, see Chapter 13.2.1.6.1: Interrupt Sources */
#define MP2128UC_IRQ_INTC0                    0 
#define MP2128UC_IRQ_INTC1                    1 

/*= CPU FMPLL bits =*/
#define MP2128UC_FMPLL_SYNCR_MFD_MASK       0x07000000
#define MP2128UC_FMPLL_SYNCR_RFD_MASK       0x00380000
/*= CPU FMPLL clock frequencies =*/
#define MP2128UC_FSYS_REF                   8192000         /* Hz */

/**************************************
 * CHANNEL (PIN) ASSIGNMENTS
 * 
 * These must be uniquely assigned, of course.
 **************************************/

#define MP2128_ETPU_GPIO_INPUT_CHAN          4
#define MP2128_ETPU_GPIO_OUTPUT_CHAN        16
#define MP2128UC_ETPU_PPA_SERVO_IN_1_CHAN   17
#define MP2128UC_FPGA_UART_RX_CHAN          18 /* P1 pin 12 */
#define MP2128UC_ETPU_PPA_SERVO_IN_2_CHAN   20
#define MP2128UC_ETPU_SERVO_TEST1_CHAN      22
#define MP2128UC_FPGA_UART_TX_CHAN          28 /* P1 pin 16 */
#define MP2128UC_DEBUG_UART_TX_CHAN         30 /* P2 pin  4 */
#define MP2128UC_DEBUG_UART_RX_CHAN         31 /* P2 pin 21 */
//#define MP2128UC_ETPU_SERVO_TEST1           31
//#define MP2128UC_ETPU_SERVO_TEST2           4

/******************************************
 * LEVEL/PRIORITY ASSIGNMENTS
 * 
 * Each combination of level and priority must be unique.
 ******************************************/

#define MP2128UC_FPGA_UART_TX_LEVEL         MP2128UC_IRQ_LEVEL_3
#define MP2128UC_FPGA_UART_TX_PRIORITY      MP2128UC_IRQ_PRIORITY_3

#define MP2128UC_FPGA_UART_RX_LEVEL         MP2128UC_IRQ_LEVEL_3
#define MP2128UC_FPGA_UART_RX_PRIORITY      MP2128UC_IRQ_PRIORITY_2

#define MP2128UC_DEBUG_UART_TX_LEVEL        MP2128UC_IRQ_LEVEL_3
#define MP2128UC_DEBUG_UART_TX_PRIORITY     MP2128UC_IRQ_PRIORITY_1

#define MP2128UC_DEBUG_UART_RX_LEVEL        MP2128UC_IRQ_LEVEL_3
#define MP2128UC_DEBUG_UART_RX_PRIORITY     MP2128UC_IRQ_PRIORITY_0

#define MP2128UC_SERVO_RX_LEVEL             MP2128UC_IRQ_LEVEL_5
#define MP2128UC_SERVO_RX_PRIORITY          MP2128UC_IRQ_PRIORITY_5

/**************************************
 * SERIAL PORT PARAMETERS
 **************************************/

#define UART_BUFFER_SIZE                    512

#define MP2128UC_UART_PARITY_NONE           0
#define MP2128UC_UART_PARITY_ODD            1
#define MP2128UC_UART_PARITY_EVEN           2

#define MP2128UC_FPGA_UART_BAUD             38400
#define MP2128UC_FPGA_UART_SIZE             8
#define MP2128UC_FPGA_UART_PARITY           MP2128UC_UART_PARITY_NONE

#define MP2128UC_DEBUG_UART_BAUD            38400
#define MP2128UC_DEBUG_UART_SIZE            8
#define MP2128UC_DEBUG_UART_PARITY          MP2128UC_UART_PARITY_NONE



/* eTPU Servo related constants */
#define MP2128UC_ETPU_QOM_HITIME_DEFAULT    3000
#define MP2128UC_SERVO_OUT_1MS              9216
#define MP2128UC_SERVO_IN_1MS               92160
#define MP2128UC_RC_RX_1MS                  9216
#define SERVO_OUT_1MS                       2000
#define SERVO_OUT_1_5MS                     3000
#define SERVO_OUT_2MS                       4000


#define MP2128UC_MAX_SERVO_HIGH             4800
#define MP2128UC_MIN_SERVO_HIGH             1200

#define MP2128UC_ETPU_CHANNEL_COUNT         32


/* eTPU QOM common function parameters */
#define MP2128UC_ETPU_QOM_PRIORITY          FS_ETPU_PRIORITY_HIGH
#define MP2128UC_ETPU_QOM_MODE              FS_ETPU_QOM_SINGLE_SHOT
#define MP2128UC_ETPU_QOM_SIGLE_SHORT       FS_ETPU_QOM_SINGLE_SHOT
#define MP2128UC_ETPU_QOM_TIME_BASE         FS_ETPU_TCR2
#define MP2128UC_ETPU_QOM_INIT_PIN          FS_ETPU_QOM_INIT_PIN_HIGH
#define MP2128UC_ETPU_QOM_FIRST_MATCH       FS_ETPU_QOM_IMMEDIATE
#define MP2128UC_ETPU_QOM_PIN_HIGH          FS_ETPU_PIN_HIGH
#define MP2128UC_ETPU_QOM_PIN_LOW           FS_ETPU_PIN_LOW
#define MP2128UC_ETPU_QOM_EVENT_SIZE        1

/* PPA common function parameters */
#define MP2128UC_ETPU_PPA_PRIORIY           FS_ETPU_PRIORITY_HIGH    
#define MP2128UC_ETPU_PPA_TIME_BASE         FS_ETPU_TCR2
#define MP2128UC_ETPU_PPA_MODE              FS_ETPU_PPA_HIGH_PULSE
#define MP2128UC_ETPU_PPA_RC_MAX_COUNT      1
#define MP2128UC_ETPU_PPA_PPS_MAX_COUNT     1
#define MP2128UC_ETPU_PPA_MAX_COUNT         10
#define MP2128UC_ETPU_PPA_SAMPLE_TIME       100000

/************************************************************
 * Field IDs                                                *
 ************************************************************/
 
#define FIELD_ID_ENABLE_TELEMETRY           116

#endif /* #include guard */
