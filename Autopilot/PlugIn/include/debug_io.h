#ifndef DEBUG_IO_H
#define DEBUG_IO_H

#include <string.h>
#include "interfaces.h"

/**
 * @brief Like the common itoa function, only for base 10 only.
 * @param num
 * @param str
 * Our own implementation since the compiler lacks it.  Public only so it can be unit tested.
 */
void itoa10(int num, char* str);

void print_debug_str(const struct debug_interface_t* dbg, const char* str, const unsigned len);

void print_debug_int(const struct debug_interface_t* dbg, const int value);

#define PRINT_DEBUG_S(debug_interface_p, message) \
do { \
    (debug_interface_p)->send_text(__FILE__, strlen(__FILE__)); \
    (debug_interface_p)->send_text(":", 1); \
    print_debug_int((debug_interface_p), __LINE__); \
    (debug_interface_p)->send_text(" ", 1); \
    print_debug_str((debug_interface_p), (message), strlen(message)); \
    (debug_interface_p)->send_text("\r\n", 2); \
} while(false)

#define PRINT_DEBUG_I(debug_interface_p, value) \
do { \
    (debug_interface_p)->send_text(__FILE__, strlen(__FILE__)); \
    (debug_interface_p)->send_text(":", 1); \
    print_debug_int((debug_interface_p), __LINE__); \
    (debug_interface_p)->send_text(" ", 1); \
    print_debug_int((debug_interface_p), (value)); \
    (debug_interface_p)->send_text("\r\n", 2); \
} while(false)

#endif /* #include guard. */
