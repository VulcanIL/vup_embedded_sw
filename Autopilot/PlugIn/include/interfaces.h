#ifndef INTERFACES_H_
#define INTERFACES_H_

#ifdef __m68k__ /* mp2128 cross-compiler. */
#include <typedefs.h>
#elif defined __CYGWIN__ /* Cygwin native compiler. */
#include <stdint.h>
#else
#error Unknown compiler!  You need to include standard integer typedefs.
#endif
#include <stdbool.h>

/*********************************************************
 * Byte-Oriented I/O
 *********************************************************/

struct byte_channel_interface_t {
    int (*receive)(unsigned char* const byteBuffer, unsigned* const bytesReturned);
    int (*send)(const unsigned char byteBuffer, unsigned* const bytesConsumed);
};

/*********************************************************
 * Time Measurement
 *********************************************************/

/**
 * Note that the get_cycle function intentionally returns a uint32, since at 30 cycles
 * per second, uint16 would give us less than an hour of time before wrapping around.
 * It returns the number of 30Hz cycles since some arbitrary time in the past.
 */
struct clock_interface_t {
    uint32_t (*get_cycle)(void);
};

/*********************************************************
 * FPGA Command Processing
 *********************************************************/

enum {
    kFPGACommandSize = 6,
    //kFPGAResponseSize = 4
    kFPGAResponseSize = 2
};

typedef enum {
    kCommandProcessedSuccessfully,
    kCommandTransmitFailed,
    kCommandEchoReadTimedOut,
    kCommandEchoReadFailed,
    kCommandEchoCorrupted,
    kDataReadTimedOut,
    kDataReadFailed
} command_result_t;

struct fpga_queue_entry_t;

typedef void (*command_queue_callback_t)(
    const struct fpga_queue_entry_t* entry,
    const command_result_t result);

struct fpga_command_t {
    unsigned char array[kFPGACommandSize];
};

struct fpga_response_t {
    unsigned char array[kFPGAResponseSize];
};

struct fpga_queue_entry_t {
    struct fpga_command_t command;
    struct fpga_response_t response;
    command_queue_callback_t callback;
    void* user_data;
};

struct fpga_queue_interface_t {
    int (*queue_command)(const struct fpga_queue_entry_t entry);
    int (*dequeue_command)(struct fpga_queue_entry_t* const entry, bool* const gotEntry);
};

/*********************************************************
 * Error Reporting
 *********************************************************/

struct debug_interface_t {
    void (*send_text)(const char*, const unsigned);
};

/*********************************************************
 * Telemetry
 *********************************************************/

 struct telemetry_interface_t {
     void (*write_telemetry)(const uint32_t measurand_id, const uint32_t data);
 };

#endif /* #include guard. */
