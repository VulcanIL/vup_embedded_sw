#ifndef TELEMETRY_TABLES_H_
#define TELEMETRY_TABLES_H_

#include "interfaces.h"

void initialize_telemetry_tables(const struct debug_interface_t *dbg);

#endif /* #include guard. */
