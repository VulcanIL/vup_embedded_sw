#ifndef FPGA_INTERFACE_H_
#define FPGA_INTERFACE_H_

#include "interfaces.h"

void run_fpga_interface(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg);
    
void reset_fpga_interface(const struct clock_interface_t* clock);

#endif /* #include guard */
