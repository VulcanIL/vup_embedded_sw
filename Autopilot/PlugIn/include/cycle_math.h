#ifndef CYCLE_MATH_H_
#define CYCLE_MATH_H_

#include "interfaces.h"

uint32_t cycle_difference(const uint32_t later, const uint32_t earlier);

#endif /* #include guard. */
