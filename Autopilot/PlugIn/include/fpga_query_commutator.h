#ifndef FPGA_QUERY_COMMUTATOR_H_
#define FPGA_QUERY_COMMUTATOR_H_

#include "interfaces.h"

 /**
  * @brief Runs the FPGA query commutator finite state machine.
  * 
  * @warning This function will keep a reference to the debug_interface_t and
  * telemetry_interface_t structs to use once the command has been executed,
  * so they MUST NOT BE EPHEMERAL.
  * 
  * @param queue
  * @param tlm_interface
  * @param dbg
  */
void run_fpga_query_commutator(
    const struct fpga_queue_interface_t* queue,
    const struct telemetry_interface_t* tlm_interface,
    const struct debug_interface_t* dbg);

void reset_fpga_query_commutator();

#endif /* #include guard. */
