#ifndef MOCK_FPGA_COMMAND_QUEUE_HPP_
#define MOCK_FPGA_COMMAND_QUEUE_HPP_

#include <deque>
#include "interfaces.h"

class MockFPGACommandQueue
{
private:

    MockFPGACommandQueue();
    
    virtual ~MockFPGACommandQueue();
    
    static MockFPGACommandQueue* _instance;
    
    std::deque<struct fpga_queue_entry_t> _queue;
    
    static int _queueFPGACommand(const struct fpga_queue_entry_t entry);
    
    static int _dequeueFPGACommand(struct fpga_queue_entry_t* const entry, bool* const gotEntry);
    
public:

    static MockFPGACommandQueue& instance();
    
    static const struct fpga_queue_interface_t queueInterface;
    
    void clear();
    
    unsigned getSize() const;
};

#endif // #include guard
