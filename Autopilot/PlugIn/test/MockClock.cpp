#include "MockClock.hpp"

MockClock* MockClock::_instance = nullptr;

MockClock::MockClock()
{
}

MockClock::~MockClock()
{
}

MockClock& MockClock::instance()
{
    if(nullptr == _instance) {
        _instance = new MockClock;
        ::testing::Mock::AllowLeak(_instance);
    }
    return *_instance;
}

uint32_t MockClock::_getCycle()
{
    return instance().getCycle();
}

const clock_interface_t MockClock::clockInterface = {
    .get_cycle = _getCycle
};
