#include <gtest/gtest.h>
#include <strings.h>
#include <random>
extern "C" {
#include "fpga_command_queue.h"
}

namespace {
    
    class FPGACommandQueueTest : public ::testing::Test
	{
    public:
    
        virtual void TearDown()
        {
            fpga_command_queue_clear();
        }
        
    protected:
    
        static void _randomlyFillByteArray(unsigned char* const array, const unsigned len)
        {
            for(unsigned i = 0; i < len; ++i) {
                array[i] = _uni(_rng);
            }
        }
        
        static void _randomlyFillQueueEntry(fpga_queue_entry_t* entry)
        {
            _randomlyFillByteArray(entry->command.array, kFPGACommandSize);
            _randomlyFillByteArray(entry->response.array, kFPGAResponseSize);
        }
        
    private:
    
        static std::random_device _rd; // only used once to initialise (seed) engine
        static std::mt19937 _rng;      // random-number engine used (Mersenne-Twister in this case)
        static std::uniform_int_distribution<int> _uni; // guaranteed unbiased
    };
    
    // This was copped from:
    // https://stackoverflow.com/questions/5008804/generating-random-integer-from-a-range
    std::random_device FPGACommandQueueTest::_rd;
    std::mt19937 FPGACommandQueueTest::_rng(FPGACommandQueueTest::_rd());
    std::uniform_int_distribution<int> FPGACommandQueueTest::_uni(0, 2^8-1);
}

TEST_F(FPGACommandQueueTest, test_push_and_get)
{
    fpga_queue_entry_t writeEntry = {
        .command = {0},
        .response = {0}
    };
    _randomlyFillQueueEntry(&writeEntry);
    
    ASSERT_EQ(0, fpga_command_queue_put(writeEntry));
    
    fpga_queue_entry_t readEntry = {
        .command = {0},
        .response = {0}
    };
    
    bool gotEntry = false;
    
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_TRUE(gotEntry);
    
    // Compare queue entries.
    for(unsigned i = 0; i < kFPGACommandSize; ++i) {
        ASSERT_EQ(writeEntry.command.array[i], readEntry.command.array[i]);
    }
    for(unsigned i = 0; i < kFPGAResponseSize; ++i) {
        ASSERT_EQ(writeEntry.response.array[i], readEntry.response.array[i]);
    }
    
    // There should be nothing left in the queue.
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_FALSE(gotEntry);
    
    // There should still be nothing left in the queue.
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_FALSE(gotEntry);
}

TEST_F(FPGACommandQueueTest, test_ordering)
{
    const unsigned testCount = kFPGACommandQueueCapacity / 2; // Don't cause an overflow.
    fpga_queue_entry_t writeEntries[testCount];
    
    // Put stuff into the queue.
    for(unsigned i = 0; i < testCount; ++i) {
        _randomlyFillQueueEntry(&writeEntries[i]);
        ASSERT_EQ(0, fpga_command_queue_put(writeEntries[i]));
    }
    
    // Get stuff back from the queue.
    for(unsigned i = 0; i < testCount; ++i) {
        fpga_queue_entry_t readEntry = {
            .command = {0},
            .response = {0}
        };
        bool gotEntry = false;
        ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
        ASSERT_TRUE(gotEntry);
        for(unsigned j = 0; j < kFPGACommandSize; ++j) {
            ASSERT_EQ(writeEntries[i].command.array[j], readEntry.command.array[j]);
        }
        for(unsigned j = 0; j < kFPGAResponseSize; ++j) {
            ASSERT_EQ(writeEntries[i].response.array[j], readEntry.response.array[j]);
        }
    }
    
    // The queue should now be empty.
    fpga_queue_entry_t readEntry;
    bool gotEntry = true;
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_FALSE(gotEntry);
}

TEST_F(FPGACommandQueueTest, test_overflow)
{
    const unsigned testCount = kFPGACommandQueueCapacity * 2; // Cause an overflow.
    fpga_queue_entry_t writeEntries[testCount];
    
    // Put stuff into the queue.
    for(unsigned i = 0; i < testCount; ++i) {
        _randomlyFillQueueEntry(&writeEntries[i]);
        if(i < kFPGACommandQueueCapacity) {
            ASSERT_EQ(0, fpga_command_queue_put(writeEntries[i]));
        }
        else {
            ASSERT_NE(0, fpga_command_queue_put(writeEntries[i]));
        }
    }
    
    // Get stuff back from the queue.
    for(unsigned i = 0; i < kFPGACommandQueueCapacity; ++i) {
        fpga_queue_entry_t readEntry = {
            .command = {0},
            .response = {0}
        };
        bool gotEntry = false;
        ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
        ASSERT_TRUE(gotEntry);
        for(unsigned j = 0; j < kFPGACommandSize; ++j) {
            ASSERT_EQ(writeEntries[i].command.array[j], readEntry.command.array[j]);
        }
        for(unsigned j = 0; j < kFPGAResponseSize; ++j) {
            ASSERT_EQ(writeEntries[i].response.array[j], readEntry.response.array[j]);
        }
    }
    
    // The queue should now be empty.
    fpga_queue_entry_t readEntry;
    bool gotEntry = false;
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_FALSE(gotEntry);
}

TEST_F(FPGACommandQueueTest, test_underflow)
{
    // Underflow the queue.
    for(unsigned i = 0; i < 10; ++i) {
        fpga_queue_entry_t readEntry;
        bool gotEntry = true;
        ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
        ASSERT_FALSE(gotEntry);
    }
    
    // Put and get an antry to make sure the queue didn't get corrupted.
    fpga_queue_entry_t writeEntry = {
        .command = {0},
        .response = {0}
    };
    _randomlyFillQueueEntry(&writeEntry);
    ASSERT_EQ(0, fpga_command_queue_put(writeEntry));
    
    fpga_queue_entry_t readEntry = {
        .command = {0},
        .response = {0}
    };
    bool gotEntry = false;
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_TRUE(gotEntry);
    
    // Compare queue entries.
    for(unsigned i = 0; i < kFPGACommandSize; ++i) {
        ASSERT_EQ(writeEntry.command.array[i], readEntry.command.array[i]);
    }
    for(unsigned i = 0; i < kFPGAResponseSize; ++i) {
        ASSERT_EQ(writeEntry.response.array[i], readEntry.response.array[i]);
    }
}

TEST_F(FPGACommandQueueTest, test_user_data_integrity)
{
    int value1 = 4;
    int value2 = 22;
    // Put a couple of entries into the queue.
    fpga_queue_entry_t writeEntry1 = {
        .command = {0},
        .response = {0},
        .callback = nullptr,
        .user_data = &value1
    };
    fpga_queue_entry_t writeEntry2 = {
        .command = {0},
        .response = {0},
        .callback = nullptr,
        .user_data = &value2
    };
    _randomlyFillQueueEntry(&writeEntry1);
    _randomlyFillQueueEntry(&writeEntry2);
    ASSERT_EQ(0, fpga_command_queue_put(writeEntry1));
    ASSERT_EQ(0, fpga_command_queue_put(writeEntry2));
    
    // Read the two entries back and make sure they match.
    fpga_queue_entry_t readEntry;
    bzero(&readEntry, sizeof(readEntry));
    bool gotEntry = false;
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_TRUE(gotEntry);
    ASSERT_EQ(writeEntry1.user_data, &value1);
    bzero(&readEntry, sizeof(readEntry));
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_TRUE(gotEntry);
    ASSERT_EQ(writeEntry2.user_data, &value2);
}

TEST_F(FPGACommandQueueTest, test_clear)
{
    // Put an entry into the queue.
    fpga_queue_entry_t writeEntry1 = {
        .command = {0},
        .response = {0}
    };
    _randomlyFillQueueEntry(&writeEntry1);
    ASSERT_EQ(0, fpga_command_queue_put(writeEntry1));
    
    // Clear the queue and attempt to fetch a command to ensure that
    // the queue is empty.
    fpga_command_queue_clear();
    fpga_queue_entry_t readEntry = {
        .command = {0},
        .response = {0}
    };
    bool gotEntry = true;
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_FALSE(gotEntry);
    
    // Put and get an entry to make sure the queue wasn't corrupted.
    fpga_queue_entry_t writeEntry2 = {
        .command = {0},
        .response = {0}
    };
    _randomlyFillQueueEntry(&writeEntry1);
    ASSERT_EQ(0, fpga_command_queue_put(writeEntry2));
    ASSERT_EQ(0, fpga_command_queue_get(&readEntry, &gotEntry));
    ASSERT_TRUE(gotEntry);
    
    // Compare queue entries.
    for(unsigned i = 0; i < kFPGACommandSize; ++i) {
        ASSERT_EQ(writeEntry2.command.array[i], readEntry.command.array[i]);
    }
    for(unsigned i = 0; i < kFPGAResponseSize; ++i) {
        ASSERT_EQ(writeEntry2.response.array[i], readEntry.response.array[i]);
    }
}

TEST_F(FPGACommandQueueTest, test_get_size)
{
    const unsigned testCount = kFPGACommandQueueCapacity / 2; // Don't cause an overflow.
    
    for(int i = 0; i < testCount; ++i) {
        
        ASSERT_EQ(i, fpga_command_queue_size());
        
        // Put an entry into the queue.
        fpga_queue_entry_t writeEntry1 = {
            .command = {0},
            .response = {0}
        };
        _randomlyFillQueueEntry(&writeEntry1);
        ASSERT_EQ(0, fpga_command_queue_put(writeEntry1));
        
        // Check the queue size after the command has been added.
        
        ASSERT_EQ(i + 1, fpga_command_queue_size());
    }
}
