#include "MockDebugInterface.hpp"

MockDebugInterface* MockDebugInterface::_instance = nullptr;

MockDebugInterface::MockDebugInterface()
{
}

MockDebugInterface::~MockDebugInterface()
{
}

MockDebugInterface& MockDebugInterface::instance()
{
    if(nullptr == _instance) {
        _instance = new MockDebugInterface;
    }
    return *_instance;
}

void MockDebugInterface::_sendText(const char* str, const unsigned len)
{
    using namespace std;
    instance()._os << string(str, static_cast<size_t>(len));
}

std::string MockDebugInterface::getOutput()
{
    return _os.str();
}

void MockDebugInterface::clear()
{
    /* See https://stackoverflow.com/questions/5288036/how-to-clear-ostringstream */
    _os.str("");
    _os.clear();
}

const debug_interface_t MockDebugInterface::debugInterface = {
    .send_text = _sendText
};
