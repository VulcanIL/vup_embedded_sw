#include <gtest/gtest.h>
#include <string>
#include <cstring>
#include <sstream>
#include <climits>
extern "C" {
#include "debug_io.h"
}
#include "MockDebugInterface.hpp"

namespace {
	
	class DebugIoTest : public ::testing::Test
	{
	public:
	
		virtual void TearDown()
		{
            MockDebugInterface::instance().clear();
		}
	};
}

TEST_F(DebugIoTest, test_print_debug_str_happy_path)
{
	const char* testString = "Fourscore and seven years ago";
	print_debug_str(&MockDebugInterface::debugInterface, testString, strlen(testString));
	ASSERT_EQ(std::string(testString), MockDebugInterface::instance().getOutput());
}

TEST_F(DebugIoTest, test_print_debug_str_respects_len)
{
	print_debug_str(&MockDebugInterface::debugInterface, "foobar", 4);
    ASSERT_EQ(std::string("foob"), MockDebugInterface::instance().getOutput());
}

TEST_F(DebugIoTest, test_print_debug_str_allows_zero_len)
{
	print_debug_str(&MockDebugInterface::debugInterface, "foobar", 0);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(DebugIoTest, test_print_debug_i_happy_path)
{
	const int testValue = 1234;
	print_debug_int(&MockDebugInterface::debugInterface, testValue);
    std::ostringstream os;
    os << testValue;
	ASSERT_EQ(os.str(), MockDebugInterface::instance().getOutput());
}

TEST_F(DebugIoTest, test_print_debug_concatenate)
{
	print_debug_str(&MockDebugInterface::debugInterface, "foo", 3);
	print_debug_int(&MockDebugInterface::debugInterface, 1);
	print_debug_str(&MockDebugInterface::debugInterface, "bar", 3);
	print_debug_int(&MockDebugInterface::debugInterface, 3);
    ASSERT_EQ(std::string("foo1bar3"), MockDebugInterface::instance().getOutput());
}

TEST_F(DebugIoTest, test_print_debug_s_macro)
{
    const char* testString = "We the people in order to form a more perfect union";
	
	// These next two statements MUST appear on the same line!
	const int lineNumber = __LINE__; PRINT_DEBUG_S(&MockDebugInterface::debugInterface, testString);
	
	using namespace std;
	ostringstream os;
	os << __FILE__ << ":" << lineNumber << " " << testString << "\r\n";
	
	ASSERT_EQ(os.str(), MockDebugInterface::instance().getOutput());
}

TEST_F(DebugIoTest, test_print_debug_i_macro)
{
    const int testValue = 9182;
	
	// These next two statements MUST appear on the same line!
	const int lineNumber = __LINE__; PRINT_DEBUG_I(&MockDebugInterface::debugInterface, testValue);
	
	using namespace std;
	ostringstream os;
	os << __FILE__ << ":" << lineNumber << " " << testValue << "\r\n";
	
	ASSERT_EQ(os.str(), MockDebugInterface::instance().getOutput());
}

TEST_F(DebugIoTest, test_itoa10)
{
    const int values[] = {
        0,
        1,
        -1,
        1234,
        -1234,
        INT_MAX,
        INT_MIN,
        INT_MAX - 1,
        INT_MIN + 1,
    };
    
    enum { kBufferSize = sizeof(int) * 8 + 1 };
    char buffer[kBufferSize];
    
    for(auto val : values) {
        std::ostringstream os;
        os << val;
        itoa10(val, buffer);
        ASSERT_EQ(os.str(), std::string(buffer));
    }
}
