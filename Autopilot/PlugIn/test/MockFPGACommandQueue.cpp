#include "MockFPGACommandQueue.hpp"

MockFPGACommandQueue* MockFPGACommandQueue::_instance = nullptr;

MockFPGACommandQueue::MockFPGACommandQueue()
{
}

MockFPGACommandQueue::~MockFPGACommandQueue()
{
}

MockFPGACommandQueue& MockFPGACommandQueue::instance()
{
    if(nullptr == _instance) {
        _instance = new MockFPGACommandQueue;
    }
    return *_instance;
}
    
int MockFPGACommandQueue::_queueFPGACommand(const struct fpga_queue_entry_t entry)
{
    instance()._queue.push_back(entry);
    return 0;
}
    
int MockFPGACommandQueue::_dequeueFPGACommand(struct fpga_queue_entry_t* const entry, bool* const gotEntry)
{
    *gotEntry = (instance()._queue.size() > 0);
    if(*gotEntry) {
        *entry = instance()._queue.front();
        instance()._queue.pop_front();
    }
    return 0;
}

void MockFPGACommandQueue::clear()
{
    _queue.clear();
}

unsigned MockFPGACommandQueue::getSize() const
{
    return _queue.size();
}

const struct fpga_queue_interface_t MockFPGACommandQueue::queueInterface = {
    .queue_command = _queueFPGACommand,
    .dequeue_command = _dequeueFPGACommand
};
