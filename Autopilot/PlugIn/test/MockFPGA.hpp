#ifndef MOCK_FPGA_HPP_
#define MOCK_FPGA_HPP_

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "interfaces.h"

class MockFPGA
{
private:

    MockFPGA();
    
    virtual ~MockFPGA();

    static MockFPGA* _instance;
    
    static int _supplyInput(unsigned char* const c, unsigned* const charsSupplied);
    
    static int _collectOutput(const unsigned char c, unsigned* const charsConsumed);
    
public:
    
    static MockFPGA& instance();
    
    MOCK_METHOD2(putByte, int(const unsigned char, unsigned* const));
    MOCK_METHOD2(getByte, int(unsigned char* const, unsigned* const));
    
    static const byte_channel_interface_t fpgaByteChannel;
};

#endif // #include guard.
