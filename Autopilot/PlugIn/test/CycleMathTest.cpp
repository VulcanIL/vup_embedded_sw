#include <gtest/gtest.h>
extern "C" {
#include "cycle_math.h"
}

TEST(CycleMathTest, testWithZeroDifference)
{
    const uint32_t later = 234;
    const uint32_t earlier = 234;
    
    ASSERT_EQ(0, cycle_difference(later, earlier));
}

TEST(CycleMathTest, testWithoutWraparound)
{
    const uint32_t later = 1000;
    const uint32_t earlier = 234;
    
    ASSERT_EQ(1000 - 234, cycle_difference(later, earlier));
}

TEST(CycleMathTest, testWithSimpleWraparound)
{
    const uint32_t later = 234;
    const uint32_t earlier = 1000;
    
    ASSERT_EQ(4294966530UL, cycle_difference(later, earlier));
}

TEST(CycleMathTest, testWithMaximumWraparound)
{
    const uint32_t later = 234;
    const uint32_t earlier = 235;
    
    ASSERT_EQ(4294967295UL, cycle_difference(later, earlier));
}

TEST(CycleMathTest, testJustBeforeWraparound)
{
    const uint32_t earlier = 4294967294UL;
    const uint32_t later = 4294967295UL;
    
    ASSERT_EQ(1, cycle_difference(later, earlier));
}

TEST(CycleMathTest, testJustAfterWraparound)
{
    const uint32_t earlier = 4294967295UL;
    const uint32_t later = 0;
    
    ASSERT_EQ(1, cycle_difference(later, earlier));
}
