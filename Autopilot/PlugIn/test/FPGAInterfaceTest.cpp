#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <cstdint>
extern "C" {
#include "fpga_interface.h"
}
#include "MockFPGA.hpp"
#include "MockFPGACommandQueue.hpp"
#include "MockClock.hpp"
#include "MockDebugInterface.hpp"

using namespace testing;

namespace {
    
    struct CallbackInfo {
        struct fpga_queue_entry_t queueEntry;
        command_result_t commandResult;
    };
    
    std::deque<CallbackInfo> _savedCallbacks;
    
    struct fpga_queue_entry_t _savedCallbackCommand;
    
    std::deque<uint8_t> _savedOutput;
    
    void _appendToSavedOutput(const unsigned char c, unsigned* const charsConsumed)
    {
        _savedOutput.push_back(c);
        *charsConsumed = 1;
    }
    
    void _appendToSavedCallbacks(const struct fpga_queue_entry_t* entry, const command_result_t result)
    {
        CallbackInfo newInfo = {
            .queueEntry = *entry,
            .commandResult = result
        };
        _savedCallbacks.push_back(newInfo);
    }

	class FPGAInterfaceTest : public ::testing::Test
	{
	public:
	
        void TearDown()
        {
            EXPECT_CALL(MockClock::instance(), getCycle())
                .WillOnce(Return(0));
            reset_fpga_interface(&MockClock::clockInterface);
            MockFPGACommandQueue::instance().clear();
            MockDebugInterface::instance().clear();
            _savedOutput.clear();
            _savedCallbacks.clear();
        }
	};
}

TEST_F(FPGAInterfaceTest, test_simple_transmit)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'W', 'P', 'Z'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will claim there are no chars ready
    // to be read, and thus halt the state machine.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(0));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // The command should now be in the output buffer.
    ASSERT_EQ(kFPGACommandSize, _savedOutput.size());
    for(unsigned i = 0; i < kFPGACommandSize; ++i) {
        ASSERT_EQ(command.command.array[i], _savedOutput[i]);
    }
    
    // Expect no callbacks or error messages.
    ASSERT_EQ(0, _savedCallbacks.size());
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_transmit_and_receive_write_command)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'W', 'P', 'Z'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo the command back, and then
    // expect a callback with a success status.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(0));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('W'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('P'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('Z'), SetArgPointee<1>(1), Return(0)));
    
    // Run the state machine.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandProcessedSuccessfully, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_error_on_transmit_command)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'W', 'P', 'Z'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will return an error code on transmit.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(0));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillOnce(Return(1));
    
    // Run the state machine.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandTransmitFailed, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_timeout_on_write_command_echo)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'W', 'P', 'Z'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // stop providing characters.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will get stuck after the second byte.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // Advance the cycle.  This should still not result in a timeout, because not
    // enough time has elapsed.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1236));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // Advance it again, and this time it should time out.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1237));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandEchoReadTimedOut, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_error_on_write_command_echo)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'W', 'P', 'Z'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // signal an error.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(Return(1))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will result in an error.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandEchoReadFailed, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_transmit_and_receive_read_command)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo the command back, then echo the data,
    // and finally expect a callback with a success status.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(0));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        // Command echo.
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('R'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('P'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('A'), SetArgPointee<1>(1), Return(0)))
        // Data.
        .WillOnce(DoAll(SetArgPointee<0>(8), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(4), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(9), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(0), SetArgPointee<1>(1), Return(0)));
    
    // Run the state machine.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandProcessedSuccessfully, _savedCallbacks[0].commandResult);
    
    // Verify that the data were saved properly.
    const uint8_t expected_data[] = {8, 4, 9, 0};
    for(unsigned i = 0; i < kFPGAResponseSize; ++i) {
        ASSERT_EQ(expected_data[i], _savedCallbacks[0].queueEntry.response.array[i]);
    }
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_timeout_on_read_command_echo)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // stop providing characters.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('R'), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will get stuck after the fourth byte.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // Advance the cycle.  This should still not result in a timeout, because not
    // enough time has elapsed.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1236));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // Advance it again, and this time it should time out.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1237));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandEchoReadTimedOut, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_error_on_read_command_echo)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // signal an error.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(Return(1))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will result in an error.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandEchoReadFailed, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_timeout_on_read_data)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo the command back, but then stop
    // in the middle of the data.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        // Command echo.
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('R'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('P'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('A'), SetArgPointee<1>(1), Return(0)))
        // Data.
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will get stuck after the second data byte.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // Advance the cycle.  This should still not result in a timeout, because not
    // enough time has elapsed.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1236));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    
    // Advance it again, and this time it should time out.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1237));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kDataReadTimedOut, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_error_on_read_data)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // signal an error.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        // Command echo.
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('R'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('P'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('A'), SetArgPointee<1>(1), Return(0)))
        // Data.
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillOnce(Return(1))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will result in an error.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kDataReadFailed, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_garbled_command_echo)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // return a bad character.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('W'), SetArgPointee<1>(1), Return(0)))
        // These won't get read until the state machine purges its input, but they
        // will be read eventually.
        .WillOnce(DoAll(SetArgPointee<0>('P'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('A'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(2), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(3), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(4), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will generate an error.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandEchoCorrupted, _savedCallbacks[0].commandResult);
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_duration_of_input_purge)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'R', 'P', 'A'},
        .response = {0},
        .callback = _appendToSavedCallbacks
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo some of the command back, but then
    // return a bad character.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1234));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('W'), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
    
    // Run the state machine.  This will generate an error and put us into the purge
    // state.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, _savedCallbacks.size());
    ASSERT_EQ(kCommandEchoCorrupted, _savedCallbacks[0].commandResult);
    
    // In the purge state, the interface won't take commands off the queue, so put
    // one there to check later.
    ASSERT_EQ(0, MockFPGACommandQueue::instance().getSize());
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    ASSERT_EQ(1, MockFPGACommandQueue::instance().getSize());
    
    // On the next cycle we will provide an input byte, and expect it to be read.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1235));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, MockFPGACommandQueue::instance().getSize());
    
    // On the next two cycles we'll provide no inputs.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1236));
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1237));
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, MockFPGACommandQueue::instance().getSize());
    
    // On the next cycle we'll provide a bunch of junk.  It should all get consumed.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1238));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>(0), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(0), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(1), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(0), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>(2), SetArgPointee<1>(1), Return(0)))
        .WillRepeatedly(DoAll(SetArgPointee<1>(0), Return(0)));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(1, MockFPGACommandQueue::instance().getSize());
    
    // On the next cycle the state machine will be back in the idle state.  We'll
    // provide no bytes or expected read calls.  Also, the command should be taken
    // off the queue and transmitted.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(1239));
    EXPECT_CALL(MockFPGA::instance(), putByte(Eq('M'), _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), putByte(Eq('O'), _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), putByte(Eq('D'), _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), putByte(Eq('R'), _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), putByte(Eq('P'), _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), putByte(Eq('A'), _))
        .WillOnce(DoAll(Invoke(_appendToSavedOutput), Return(0)));
        
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(0, MockFPGACommandQueue::instance().getSize());
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}

TEST_F(FPGAInterfaceTest, test_no_error_if_null_callback)
{
    // Put a command into the queue.
    struct fpga_queue_entry_t command = {
        .command = {'M', 'O', 'D', 'W', 'P', 'Z'},
        .response = {0},
        .callback = nullptr
    };
    
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.queue_command(command));
    
    // Set up the test harness.  It will echo the command back.
    EXPECT_CALL(MockClock::instance(), getCycle())
        .WillRepeatedly(Return(0));
    EXPECT_CALL(MockFPGA::instance(), putByte(_, _))
        .WillRepeatedly(DoAll(Invoke(_appendToSavedOutput), Return(0)));
    EXPECT_CALL(MockFPGA::instance(), getByte(_, _))
        .WillOnce(DoAll(SetArgPointee<0>('M'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('O'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('D'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('W'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('P'), SetArgPointee<1>(1), Return(0)))
        .WillOnce(DoAll(SetArgPointee<0>('Z'), SetArgPointee<1>(1), Return(0)));
    
    // Run the state machine.  Expect no callback or error messages.
    run_fpga_interface(
        &MockFPGA::fpgaByteChannel,
        &MockFPGACommandQueue::queueInterface,
        &MockClock::clockInterface,
        &MockDebugInterface::debugInterface);
    ASSERT_EQ(0, _savedCallbacks.size());
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());
}
