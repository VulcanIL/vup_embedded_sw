#include <gtest/gtest.h>
#include <map>
extern "C" {
#include "fpga_query_commutator.h"
}
#include "MockFPGACommandQueue.hpp"
#include "MockDebugInterface.hpp"

namespace {
    
    class FPGAQueryCommutatorTest : public ::testing::Test
	{
    public:
    
        virtual void SetUp()
        {
            _collisionCount = 0;
        }
        
        virtual void TearDown()
        {
            MockFPGACommandQueue::instance().clear();
            MockDebugInterface::instance().clear();
            _telemetryCache.clear();
            reset_fpga_query_commutator();
        }
        
    protected:
    
        static void _writeTelemetry(const uint32_t measurand_id, const uint32_t data)
        {
            if(_telemetryCache.count(measurand_id) > 0) {
                _collisionCount++;
            }
            _telemetryCache[measurand_id] = data;
        }
        
        static telemetry_interface_t _tlmInterface;
        static unsigned _collisionCount;
        static std::map<uint32_t, uint32_t> _telemetryCache;
    };
    
    telemetry_interface_t FPGAQueryCommutatorTest::_tlmInterface = {
        .write_telemetry = FPGAQueryCommutatorTest::_writeTelemetry
    };
    
    unsigned FPGAQueryCommutatorTest::_collisionCount = 0;
    
    std::map<uint32_t, uint32_t> FPGAQueryCommutatorTest::_telemetryCache;
}

TEST_F(FPGAQueryCommutatorTest, test_queueing_of_request)
{
    // Queue a telemetry request.
    while(0 == MockFPGACommandQueue::instance().getSize()) {
        run_fpga_query_commutator(
            &MockFPGACommandQueue::queueInterface,
            &_tlmInterface,
            &MockDebugInterface::debugInterface);
    }
    ASSERT_EQ(1, MockFPGACommandQueue::instance().getSize());
    ASSERT_EQ(0, MockDebugInterface::instance().getOutput().size());

    // Fetch the request from the queue.
    struct fpga_queue_entry_t entry;
    bool got_entry = false;
    ASSERT_EQ(0, MockFPGACommandQueue::queueInterface.dequeue_command(&entry, &got_entry));
    ASSERT_TRUE(got_entry);
    ASSERT_EQ(0, MockFPGACommandQueue::instance().getSize());

    // Populate the entry with data.
    for(unsigned i = 0; i < kFPGAResponseSize; ++i) {
        entry.response.array[i] = i * 7;
    }

    // Run the callback.
    entry.callback(&entry, kCommandProcessedSuccessfully);

    // Verify that the data were transferred properly.
    ASSERT_EQ(0, _collisionCount);
    ASSERT_EQ(1, _telemetryCache.size());
    ASSERT_EQ(1, _telemetryCache.count(1545));
    ASSERT_EQ(*(reinterpret_cast<uint32_t*>(entry.response.array)), _telemetryCache[1545]);
}
