#include <gtest/gtest.h>
#include <string>
#include <sstream>
extern "C" {
#include "text_queue.h"
}

namespace {

    char nextChar(const char c) {
        if('z' == c) {
            return 'a';
        }
        else {
            return c + 1;
        }
    }
}

TEST(TextQueueTest, test_put_char_no_overflow)
{
    const std::string testString = "This is a test.\r\n";
    ASSERT_TRUE(testString.size() < kTextQueueCapacity);
    for(char c : testString) {
        textQueuePutChar(c);
        char buffer;
        unsigned charsFetched = 0;
        textQueueGet(&buffer, &charsFetched);
        ASSERT_EQ(1, charsFetched);
        ASSERT_EQ(c, buffer);
        textQueueGet(&buffer, &charsFetched);
        ASSERT_EQ(0, charsFetched);
    }
}

TEST(TextQueueTest, test_put_str_no_overflow)
{
    const std::string testString = "This is a test.";
    ASSERT_TRUE(testString.size() < kTextQueueCapacity);
    for(unsigned i = 1; i <= testString.size(); ++i) {
        const std::string thisTestString = testString.substr(0, i);
        // Write the string.
        textQueuePutStr(thisTestString.c_str(), i);
        // Read it back, a char at a time.
        for(unsigned j = 0; j < i; ++j) {
            char buffer;
            unsigned charsFetched = 0;
            textQueueGet(&buffer, &charsFetched);
            ASSERT_EQ(1, charsFetched);
            ASSERT_EQ(thisTestString[j], buffer);
        }
        // Make sure the queue is empty.
        char buffer;
        unsigned charsFetched = 1;
        textQueueGet(&buffer, &charsFetched);
        ASSERT_EQ(0, charsFetched);
    }
}

TEST(TextQueueTest, test_next_char)
{
    const std::string testString = "abcdefghijklmnopqrstuvwxyzabcdefghi";
    for(unsigned i = 0; i < testString.size() - 1; ++i) {
        ASSERT_EQ(testString[i + 1], nextChar(testString[i]));
    }
}

TEST(TextQueueTest, test_put_str_to_capacity)
{
    std::ostringstream testString;
    char testChar = 'a';
    // Fill the test string.
    for(unsigned i = 0; i < kTextQueueCapacity - 1; ++i) {
        testString << testChar;
        testChar = nextChar(testChar);
    }
    // Put the test string into the queue.
    textQueuePutStr(testString.str().c_str(), testString.str().size());
    // Now empty it and check the chars.
    testChar = 'a';
    for(unsigned i = 0; i < testString.str().size(); ++i) {
        char buffer;
        unsigned charsFetched = 0;
        textQueueGet(&buffer, &charsFetched);
        ASSERT_EQ(1, charsFetched);
        ASSERT_EQ(testString.str()[i], buffer);
    }
    // The queue should be empty.
    char buffer;
    unsigned charsFetched = 1;
    textQueueGet(&buffer, &charsFetched);
    ASSERT_EQ(0, charsFetched);
}

TEST(TextQueueTest, test_put_str_to_just_overflow)
{
    std::ostringstream testString;
    char testChar = 'a';
    // Fill the test string.
    for(unsigned i = 0; i < kTextQueueCapacity; ++i) {
        testString << testChar;
        testChar = nextChar(testChar);
    }
    // Put the test string into the queue.
    textQueuePutStr(testString.str().c_str(), testString.str().size());
    // Now empty it and check the chars.
    testChar = 'a';
    for(unsigned i = 0; i < testString.str().size() - 1; ++i) {
        char buffer;
        unsigned charsFetched = 0;
        textQueueGet(&buffer, &charsFetched);
        ASSERT_EQ(1, charsFetched);
        ASSERT_EQ(testString.str()[i], buffer);
    }
    // There should be a '*' in the queue to indicate it overflowed.
    char buffer;
    unsigned charsFetched = 0;
    textQueueGet(&buffer, &charsFetched);
    ASSERT_EQ(1, charsFetched);
    ASSERT_EQ('*', buffer);
    // Now the queue should be empty.
    textQueueGet(&buffer, &charsFetched);
    ASSERT_EQ(0, charsFetched);
}

TEST(TextQueueTest, test_put_str_to_way_overflow)
{
    std::ostringstream testString;
    char testChar = 'a';
    // Fill the test string.
    for(unsigned i = 0; i < 2 * kTextQueueCapacity; ++i) {
        testString << testChar;
        testChar = nextChar(testChar);
    }
    // Put the test string into the queue.
    textQueuePutStr(testString.str().c_str(), testString.str().size());
    // Now empty it and check the chars.
    testChar = 'a';
    for(unsigned i = 0; i < kTextQueueCapacity - 1; ++i) {
        char buffer;
        unsigned charsFetched = 0;
        textQueueGet(&buffer, &charsFetched);
        ASSERT_EQ(1, charsFetched);
        ASSERT_EQ(testString.str()[i], buffer);
    }
    // There should be a '*' in the queue to indicate it overflowed.
    char buffer;
    unsigned charsFetched = 0;
    textQueueGet(&buffer, &charsFetched);
    ASSERT_EQ(1, charsFetched);
    ASSERT_EQ('*', buffer);
    // Now the queue should be empty.
    textQueueGet(&buffer, &charsFetched);
    ASSERT_EQ(0, charsFetched);
}
