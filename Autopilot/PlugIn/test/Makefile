
SRCDIR = .
OBJDIR = obj
COVDIR = coverage
APPNAME = testmain.exe
VPATH = $(SRCDIR):../src
XTENDER_HOME=/cygdrive/c/Program\ Files\ \(x86\)/MicroPilot/XTENDER3.7

C_SOURCES = debug_io.c fpga_interface.c text_queue.c fpga_command_queue.c cycle_math.c \
	fpga_query_commutator.c
CPP_SOURCES = DebugIoTest.cpp FPGAInterfaceTest.cpp TextQueueTest.cpp FPGACommandQueueTest.cpp \
	CycleMathTest.cpp FPGAQueryCommutatorTest.cpp MockFPGA.cpp MockClock.cpp MockFPGACommandQueue.cpp \
	MockDebugInterface.cpp
OBJECTS = $(addprefix $(OBJDIR)/, $(C_SOURCES:.c=.o) $(CPP_SOURCES:.cpp=.o))
DEPENDS = $(OBJECTS:.o=.d)

CPPFLAGS = -I../include -ggdb --coverage
CFLAGS = $(CPPFLAGS)

$(APPNAME): $(OBJECTS)
	$(CXX) -o $@ $^ -L/usr/local/lib -lgtest_main -lgtest -lgmock -lgcov

$(OBJDIR)/%.o: %.cpp $(OBJDIR)/%.d
	@mkdir -p $(OBJDIR)
	$(CXX) -o $@ $(CPPFLAGS) -c $<

$(OBJDIR)/%.o: %.c $(OBJDIR)/%.d
	@mkdir -p $(OBJDIR)
	$(CC) -o $@ $(CFLAGS) -c $<

.PHONY: clean test lint

clean:
	rm -rf $(OBJDIR) $(COVDIR) $(APPNAME)

test: $(APPNAME)
	lcov --directory $(OBJDIR) --zerocounters --quiet
	./$(APPNAME)
	@echo -n 'Collecting coverage statistics... '
	@rm -rf $(COVDIR)
	@mkdir -p $(COVDIR)
	@lcov --directory $(OBJDIR) --capture --output-file $(COVDIR)/unit_tests.info --quiet
	@lcov --remove $(COVDIR)/unit_tests.info "/usr*" "c++/*" --output-file $(COVDIR)/unit_tests.info --quiet
	@genhtml --output-directory $(COVDIR) $(COVDIR)/unit_tests.info --quiet
	@echo done.

# This devilry is copped from the Gnu make manual:
# https://www.gnu.org/software/make/manual/make.html#Automatic-Prerequisites

$(OBJDIR)/%.d: $(SRCDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	@set -e; rm -f $@; \
	$(CXX) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,$(OBJDIR)/\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$
	
$(OBJDIR)/%.d: $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@set -e; rm -f $@; \
	$(CC) -MM $(CFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,$(OBJDIR)/\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

lint:
	@splint -I../include -I$(XTENDER_HOME) -I$(XTENDER_HOME)/mpsdk/src/mp2128/coldfire-etpu/include \
    -I$(XTENDER_HOME)/mpsdk/src/mp2128/coldfire-etpu/src -I$(XTENDER_HOME)/mpsdk/include/mp2128 \
    -I$(XTENDER_HOME)/mpsdk/src/mp2128/include -I$(XTENDER_HOME)/mpsdk/src/mp2128 \
	-sysdirs $(XTENDER_HOME):/usr/include -D__m68k__ +skip-sys-headers -DVFNPTR=void\* -preproc \
	../src/*.c ../include/*.h

include $(DEPENDS)
