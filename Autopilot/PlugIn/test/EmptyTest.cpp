
#include <gtest/gtest.h>

class Foo
{
public:
	
	int foo()
	{
		return 1;
	}
};

TEST(FooTest, testFoo)
{
	Foo foo;
	ASSERT_EQ(1, foo.foo());
}
