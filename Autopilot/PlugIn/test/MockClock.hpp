#ifndef MOCK_CLOCK_HPP_
#define MOCK_CLOCK_HPP_

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "interfaces.h"

class MockClock
{
private:

    MockClock();
    
    virtual ~MockClock();
    
    static MockClock* _instance;
    
    static uint32_t _getCycle();
    
public:

    static MockClock& instance();
    
    MOCK_METHOD0(getCycle, uint32_t());
    
    static const struct clock_interface_t clockInterface;
};

#endif // #include guard
