#ifndef MOCK_DEBUG_INTERFACE_HPP_
#define MOCK_DEBUG_INTERFACE_HPP_

#include <sstream>
#include "interfaces.h"

class MockDebugInterface : public debug_interface_t
{
private:

    MockDebugInterface();
    
    virtual ~MockDebugInterface();
    
    static MockDebugInterface* _instance;
    
    static void _sendText(const char* str, const unsigned len);
    
    std::ostringstream _os;
    
public:

    static MockDebugInterface& instance();
    
    static const struct debug_interface_t debugInterface;
    
    std::string getOutput();
    
    void clear();
};

#endif /* #include guard. */
