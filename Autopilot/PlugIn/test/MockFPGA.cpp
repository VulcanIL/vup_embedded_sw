#include "MockFPGA.hpp"

MockFPGA* MockFPGA::_instance = nullptr;

MockFPGA::MockFPGA()
{
}

MockFPGA::~MockFPGA()
{
}

MockFPGA& MockFPGA::instance()
{
    if(nullptr == _instance) {
        _instance = new MockFPGA;
        ::testing::Mock::AllowLeak(_instance);
    }
    return *_instance;
}
    
int MockFPGA::_supplyInput(unsigned char* const c, unsigned* const charsSupplied)
{
    return instance().getByte(c, charsSupplied);
}

int MockFPGA::_collectOutput(const unsigned char c, unsigned* const charsConsumed)
{
    return instance().putByte(c, charsConsumed);
}

const byte_channel_interface_t MockFPGA::fpgaByteChannel = {
    .receive = _supplyInput,
    .send = _collectOutput
};
