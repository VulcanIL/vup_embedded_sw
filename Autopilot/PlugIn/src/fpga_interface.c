#include <stdbool.h>
#include <string.h>
#include "fpga_interface.h"
#include "debug_io.h"
#include "text_queue.h"

enum {
    kFPGATimeoutCycles = 3,
    kPurgeInputDurationCycles = 5
};

static struct fpga_queue_entry_t _current_command;
static unsigned _transmit_index = 0;
static unsigned _receive_index = 0;
static uint32_t _cycle_at_start_of_current_state = 0;

static bool _is_read_command(struct fpga_queue_entry_t* entry)
{
    enum { kHeaderSize = 4 };
    static const char read_header[kHeaderSize] = {'M', 'O', 'D', 'R'};
    return (0 == memcmp(read_header, entry->command.array, kHeaderSize));
}

typedef bool (*state_function_t)(
    const struct byte_channel_interface_t*,
    const struct fpga_queue_interface_t*,
    const struct clock_interface_t*,
    const struct debug_interface_t*);

static bool _run_idle(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg);

static bool _run_transmit(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg);

static bool _run_receive_echo(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg);

static bool _run_receive_data(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg);

static bool _run_purge_input(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg);

static state_function_t _current_state = _run_idle;

static void _go_to_state(state_function_t new_state, const struct clock_interface_t* clock)
{
    _current_state = new_state;
    _cycle_at_start_of_current_state = clock->get_cycle();
}

static uint32_t _cycles_in_current_state(const struct clock_interface_t* clock)
{
    return clock->get_cycle() - _cycle_at_start_of_current_state;
}

static void _issue_callback(const command_result_t result)
{
    if(NULL != _current_command.callback) {
        _current_command.callback(&_current_command, result);
    }
}

static bool _run_idle(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg)
{
    bool got_entry = false;
    const int status = fpga_queue->dequeue_command(&_current_command, &got_entry);
    
    if(0 != status) {
        PRINT_DEBUG_I(dbg, status);
        return true;
    }
    else if(got_entry) {
        _transmit_index = 0;
        _go_to_state(_run_transmit, clock);
        return true;
    }
    else {
        return false; /* Nothing in the queue. */
    }
}

static bool _run_transmit(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg)
{
    if(_transmit_index < kFPGACommandSize) { /* We still have chars to transmit. */
        unsigned bytes_sent = 0;
        const int status = byte_channel->send(_current_command.command.array[_transmit_index], &bytes_sent);
        if(0 != status) {
            _issue_callback(kCommandTransmitFailed);
            _go_to_state(_run_idle, clock); /* Abort this command. */
            return true;
        }
        else if(0 == bytes_sent) { /* Hardware isn't ready to send message. */
            return false;
        }
        else {
            _transmit_index++;
            return true;
        }
    }
    else {
        _receive_index = 0;
        _go_to_state(_run_receive_echo, clock);
        return true;
    }
}

static bool _run_receive_echo(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg)
{
    if(_cycles_in_current_state(clock) >= kFPGATimeoutCycles) {
        _issue_callback(kCommandEchoReadTimedOut);
        _go_to_state(_run_idle, clock);
        return true;
    }
    else if(_receive_index < kFPGACommandSize) { /* Haven't seen the entire echo yet. */
        unsigned char byte_buffer;
        unsigned bytes_fetched = 0;
        const int status = byte_channel->receive(&byte_buffer, &bytes_fetched);
        if(0 != status) {
            _issue_callback(kCommandEchoReadFailed);
            _go_to_state(_run_idle, clock); /* Abort this command. */
            return true;
        }
        else if(0 == bytes_fetched) {
            /* No chars ready to be read. */
            return false;
        }
        else if(byte_buffer != _current_command.command.array[_receive_index]) {
            _issue_callback(kCommandEchoCorrupted);
            _go_to_state(_run_purge_input, clock); /* Abort this command. */
            return true;
        }
        else {
            /* We got the byte we were expecting, so keep going. */
            _receive_index++;
            return true;
        }
    }
    else if(_is_read_command(&_current_command)) {
        /* Go to read the response. */
        _go_to_state(_run_receive_data, clock);
        return true;
    }
    else {
        /* This is a write command.  We're done, go back to idle. */
        _issue_callback(kCommandProcessedSuccessfully);
        _go_to_state(_run_idle, clock);
        return true;
    }
}

static bool _run_receive_data(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg)
{
    if(_cycles_in_current_state(clock) >= kFPGATimeoutCycles) {
        _issue_callback(kDataReadTimedOut);
        _go_to_state(_run_idle, clock);
        return true;
    }
    else if(_receive_index < kFPGACommandSize + kFPGAResponseSize) { /* Haven't seen all data yet. */
        unsigned bytes_fetched;
        const unsigned response_index = _receive_index - kFPGACommandSize;
        const int status = byte_channel->receive(&_current_command.response.array[response_index], &bytes_fetched);
        if(0 != status) {
            _issue_callback(kDataReadFailed);
            _go_to_state(_run_idle, clock); /* Abort this command. */
            return true;
        }
        else if(0 == bytes_fetched) {
            /* No chars ready to be read. */
            return false;
        }
        else {
            /* We got a data byte, so keep going. */
            _receive_index++;
            return true;
        }
    }
    else {
        /* We're done reading data, so go back to idle. */
        _issue_callback(kCommandProcessedSuccessfully);
        _go_to_state(_run_idle, clock);
        return true;
    }
}

/**
 * @brief 
 * @param receiver
 * @param sender
 * @param clock
 * @return 
 * @todo This needs a bailout counter.  Actually many things probably do.
 */
static bool _run_purge_input(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg)
{
    if(_cycles_in_current_state(clock) >= kPurgeInputDurationCycles) {
        _go_to_state(_run_idle, clock);
        return true;
    }
    else {
        unsigned char byte_buffer;
        unsigned bytes_fetched;
        do {
            bytes_fetched = 0;
            (void) byte_channel->receive(&byte_buffer, &bytes_fetched);
        } while(bytes_fetched > 0);
        return false;
    }
}

void run_fpga_interface(
    const struct byte_channel_interface_t* byte_channel,
    const struct fpga_queue_interface_t* fpga_queue,
    const struct clock_interface_t* clock,
    const struct debug_interface_t* dbg)
{
    while(_current_state(byte_channel, fpga_queue, clock, dbg)) {
        // There's nothing to do in the body, it is all done in _current_state().
    }
}

void reset_fpga_interface(const struct clock_interface_t* clock)
{
    _go_to_state(_run_idle, clock);
}
