#include "fpga_query_commutator.h"
#include "debug_io.h"

#define READ_CURRENT             'C'

#define ESCL_CURRENT             'B'
#define ESCR_CURRENT             'C'
#define DDL_AMP_CURRENT          'D'
#define TRILLIUM_CURRENT         'E'
#define P400_CURRENT             'F'
#define TRILLIUM_RETRACT_CURRENT 'G'
#define PDDL_CURRENT_5V          'H'
#define PDDL_CURRENT_3_3V        'I'
#define VPB_PS_CURRENT           'J'
#define SERVO_1_CURRENT          'K'
#define SERVO_2_CURRENT          'L'
#define SERVO_3_CURRENT          'M'
#define SERVO_4_CURRENT          'N'
#define SERVO_5_CURRENT          'O'
#define SERVO_6_CURRENT          'R'

struct commutation_data_t {
    struct fpga_command_t request;
    uint32_t field_id;
    const struct telemetry_interface_t* tlm_interface;
    const struct debug_interface_t* dbg;
};

static struct commutation_data_t _commutation_array[] = {
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, ESCL_CURRENT} },
        .field_id = 1545
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, ESCR_CURRENT} },
        .field_id = 1546
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, DDL_AMP_CURRENT} },
        .field_id = 1547
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, TRILLIUM_CURRENT} },
        .field_id = 1548
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, PDDL_CURRENT_5V} },
        .field_id = 1549
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, PDDL_CURRENT_3_3V} },
        .field_id = 1550
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, VPB_PS_CURRENT} },
        .field_id = 1551
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, SERVO_1_CURRENT} },
        .field_id = 1552
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, SERVO_2_CURRENT} },
        .field_id = 1553
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, SERVO_3_CURRENT} },
        .field_id = 1554
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, SERVO_4_CURRENT} },
        .field_id = 1555
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, SERVO_5_CURRENT} },
        .field_id = 1556
    },
    {
        .request = { .array = {'M', 'O', 'D', 'R', READ_CURRENT, SERVO_6_CURRENT} },
        .field_id = 1557
    },
};

static const unsigned _commutation_count =
    sizeof(_commutation_array) / sizeof(_commutation_array[0]);
    
enum {
    kCyclesBetweenRequests = 5
};

static unsigned _cycle_counter = 0;
static unsigned _commutation_index = 0;

static void _telemetry_callback(
    const struct fpga_queue_entry_t* entry,
    const command_result_t result)
{
    struct commutation_data_t* commutation_data = (struct commutation_data_t*) entry->user_data;
    
    switch(result) {
        
    case kCommandProcessedSuccessfully:
        commutation_data->tlm_interface->write_telemetry(
            commutation_data->field_id,
            *((uint32_t *) entry->response.array));
        break;
    
    default:
        PRINT_DEBUG_I(commutation_data->dbg, result);
        break;
    }
}

void run_fpga_query_commutator(
    const struct fpga_queue_interface_t* queue,
    const struct telemetry_interface_t* tlm_interface,
    const struct debug_interface_t* dbg)
{
    if(_cycle_counter++ >= kCyclesBetweenRequests) {
        _cycle_counter = 0;
        struct commutation_data_t* commutation_data = &_commutation_array[_commutation_index];
        commutation_data->tlm_interface = tlm_interface;
        commutation_data->dbg = dbg;
        struct fpga_queue_entry_t entry;
        entry.command = commutation_data->request;
        entry.callback = _telemetry_callback;
        entry.user_data = commutation_data;
        const int status = queue->queue_command(entry);
        if(0 != status) {
            PRINT_DEBUG_I(dbg, status);
        }
        _commutation_index = (_commutation_index + 1) % _commutation_count;
    }
}

void reset_fpga_query_commutator()
{
    _cycle_counter = 0;
    _commutation_index = 0;
}
