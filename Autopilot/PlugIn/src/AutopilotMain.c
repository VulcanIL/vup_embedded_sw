/*****************************************************************************
Filename:   mp2128-example.c

Project:    1) Simulator 
            2) AutoPilot firmware codebase
            3) MicroPilot SDK

Copyright:  (c) 1998-2006 MicroPilot

Author:     Howard Loewen, Vitaliy Degtyaryov

Synopsis:   This example shows haw to initialize and test hardware of the autopilot using the plugin.
            The plugin initializes etpu gpio, etpu ppa, and etpu uart functions.
            - The etpu gpio ch.16 (P2.14) is used to show how long it takes for the autopilot
            to run the plugin. 
            - The etpu gpio ch.4 (P2.15) is used to show the effect of the 
            delay caused by the plugin on the performance of the autopilot. Increasing the 
            delay variable "ttt", the user can increase the time taken by the plugin. If a delay
            is long enough, the autopilot may slow down.  
            - The etpu ppa ch.22 (P2.13) is used to otput the servo signal of the predetermined 
            length. The user vary it by changing the variable "hightime" using the BDM.
            - The etpu ppa ch.31 (P2.21) is used to read the servo signal.
            - The etpu uart ch.28 (P1.16) is used to transmit the predermined character 'A'.
            - The etpu uart ch.18 (P1.12) is used to read from the etpu uart Rx buffer. It it is            
            An oscilloscope or a logic analyser can be used to visualize the waveforms on the above
            mentioned pins.

CVS-TAG :   $Id$

Comments:   This is a stub function file and NOT a full example so it may
            contain bare minimum implementations of all functions.
            
            The code in this file is compiled in two different fashions: 
            to run with the MicroPilot simulator and to run on the actual MP2128
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdbool.h>

#include "AutopilotDefs.h"
#include "text_queue.h"
#include "fpga_interface.h"
#include "debug_io.h"
#include "telemetry_tables.h"

#include "mpsdk/include/usercode.h"
#include "mpsdk/include/mpTypes.h"
#include "mpsdk/include/droneConsts.h"
#include "mpsdk/include/simdll.h"
#include "mpsdk/include/mpfields.h"

#include "mpsdk/src/mp2128/include/mp2128.h"

#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_util.h"
#include "mpsdk/src/mp2128/include/mp2128-etpu-vars.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_gpio.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_fpm.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_ic.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_oc.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_ppa.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_pwm.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_qom.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_spi.h"
#include "mpsdk/src/mp2128/coldfire-etpu/include/etpu_uart.h"

#define YES         1
#define NO          0
#define UNUSED(x) (x)


struct uart_record_t {
    uint16_t  rxBuffer[UART_BUFFER_SIZE];
    uint16_t  txBuffer[UART_BUFFER_SIZE];
    int32_t volatile rxInsertPtr;
    int32_t volatile rxExtractPtr;
    int32_t volatile txInsertPtr;
    int32_t volatile txExtractPtr;
    uint16_t volatile *upper_rx;
    uint16_t volatile *upper_tx;
    unsigned char rxChan;
    unsigned char txChan;
};

/* eTPU UART related variables */
static struct uart_record_t _fpga_uart_record;
static struct uart_record_t _debug_uart_record;


static const struct debug_interface_t _text_queue_dbg = {
    .send_text = textQueuePutStr
};


/****************************************************************************/
/*  PRIVATE FUNCTION PROTOTYPES                                             */
/****************************************************************************/

/* Genearal eTPU and CPU Coldfire related functions */
static int32_t getCpuSpeed( void );

static int32_t enable_usr_irq( uint16_t irq_controller, uint16_t irq_source,   
                   uint16_t  irq_level,      uint16_t irq_priority,
                   VFNPTR          irq_function );

static void mp2128_get_etpu_freq( int32_t cpu_frequency ); 


/* eTPU GPIO related functions */
static uint16_t etpuGpioInit( uint16_t channel, uint16_t priority );
static int32_t mp2128_etpu_gpio_read(int32_t channel);

/* eTPU UART related functions */
static void debugUartRxISR( void );
static void debugUartTxISR( void );
static void fpgaUartRxISR( void );
static void fpgaUartTxISR( void );
static int32_t uartTxEmpty( struct uart_record_t *uart_record);
static int32_t uartPut( int32_t character, struct uart_record_t *uart_record);
static int32_t uartGet( struct uart_record_t *uart_record );
static int32_t uartRxEmpty( struct uart_record_t *uart_record );
static int32_t uartStatus( struct uart_record_t *uart_record );

static uint16_t tpuUartInit(
    uint32_t baud,
    int32_t size,
    int32_t parity,
    unsigned char txChan,
    uint16_t txLevel,
    uint16_t txPriority,
    unsigned char rxChan,
    uint16_t rxLevel,
    uint16_t rxPriority,
    struct uart_record_t *uart_record,
    VFNPTR pit_rx_function,
    VFNPTR pit_tx_function );

static uint16_t mp2128_etpu_uart_init( uint16_t tx_channel, uint16_t rx_channel,
                                       unsigned char  priority,   uint32_t  baud_rate,  
                                       unsigned char  bits,       unsigned char  parity,
                                       uint32_t  etpu_freq );

/* eTPU Tx Servo related functions */
static void hitServoOut( uint32_t channel, int32_t hightime);
static uint16_t initServoOut( int32_t channel);
static uint16_t mp2128_etpu_qom_single_short_init(  uint16_t channel, int32_t *event_array );


/* eTPU Rx Servo related functions */
static void servoRxISR1(void);
static int32_t readRxServo( uint32_t channel );
static void readTPU_PTA( int32_t channel);
static uint16_t initRxServo(   uint32_t channel, uint32_t *upper_ppa, VFNPTR irq_function );
static uint16_t mp2128_etpu_ppa_init( uint32_t channel );

/****************************************************************************/
/*  CONSTANTS                                                               */
/****************************************************************************/


/****************************************************************************/
/*  EXTERNAL VARIABLES                                                      */
/****************************************************************************/
extern struct eTPU_struct *eTPU;
extern uint32_t fs_etpu_code_start;
extern uint32_t fs_etpu_data_ram_start;
extern uint32_t fs_etpu_data_ram_end;
extern uint32_t fs_etpu_data_ram_ext;
extern uint32_t *fs_free_param;
extern uint32_t etpu_cie_a; 

/****************************************************************************/
/*  GLOBAL VARIABLES                                                        */
/****************************************************************************/
static uint32_t *fs_free_param_address = 0;
static int32_t ltpu_servo[MP2128UC_ETPU_CHANNEL_COUNT]; /* required for eTPU Servo IN/OUT */

/****************************************************************************/
/*  PRIVATE VARIABLES                                                       */
/****************************************************************************/
static uint32_t    etpu_a_tcr1_freq;  
static uint32_t    etpu_a_tcr2_freq;
static int32_t (*saveGetVarPointer)( void **result, int32_t id);

/* eTPU Servo IN/OUT related variables */
static volatile uint16_t *upper_PPA_SCR16_RX_SERVO1;
static volatile uint16_t *upper_PPA_SCR16_RX_SERVO2;

static int32_t tpuResult[MP2128UC_ETPU_CHANNEL_COUNT];
static int32_t tpuCount[MP2128UC_ETPU_CHANNEL_COUNT];

static int32_t g30hzCount = 0;
static int32_t gLast30hzCount = 0;

/***************************************************************
 * USER TELEMETRY FIELDS                                       *
 ***************************************************************/
 
 
 
static void _put_debug_text_in_telemetry(int32_t *debug_text_field)
{
    /* Telemetry frames go out at 5 Hz, i.e. every 6 cycles. */
    if(0 == (g30hzCount % 6)) {
        static uint8_t frame_counter = 0;
        unsigned i = 0;
        unsigned chars_fetched = 0;
        *debug_text_field = frame_counter++;
        for(i = 0; i < 3; ++i) {
            char char_buf = '\0';
            textQueueGet(&char_buf, &chars_fetched);
            if(0 == chars_fetched) {
                char_buf = '\0';
            }
            *debug_text_field <<= 8;
            *debug_text_field &= 0xFFFFFF00;
            *debug_text_field |= char_buf;
        }
    }
}


/***************************************************************************/
/*!
@param  result  [in/out]    pointer to the variable in the field ID
        id      [in]        field ID

@return 0 on failure    - field ID has not been found
        1 on success    - field ID has been found

@brief
This function communicates with the autopilot and returns a pointer to the 
variable from the required field ID
****************************************************************************/
int32_t getMPVarPointer( void **result, int32_t id)
{
   return (*saveGetVarPointer)( result, id);
}

/*==========================================================================*/
/*                                                                          */
/*          EXAMPLE OF INITIALIZATION OF eTPU GPIO                          */ 
/*                                                                          */
/*==========================================================================*/

/****************************************************************************/
/*!
@param  channel     [in]    eTPU channel to be initialized  
        priority    [in]    eTPU channel priority

@return channel number      - if there is an error of initialization
        0                   - if initialization is successful

@brief
Initialization of eTPU GPIO
*****************************************************************************/
static uint16_t etpuGpioInit( uint16_t channel, uint16_t priority )
{
    int32_t err_code = 0;
    uint16_t result = 0;

    fs_free_param = *fs_free_param_address;
    err_code = fs_etpu_gpio_init ( channel, priority );                
    *fs_free_param_address = fs_free_param;

    if( err_code != 0 )
    {
        result = channel;
    }
    else
    {
        result = 0;
    }

    return result;
}

/****************************************************************************/
/*!
@param  channel     [in]    eTPU channel to be read

@return 1 if the digital input is currently high, 0 if it is currently low

@brief
Reads the state of the digital input channel and returns 1 if it is high or 0 if it is low
*****************************************************************************/
static int32_t mp2128_etpu_gpio_read(int32_t channel)
{
    int32_t state;

    fs_etpu_gpio_input_immed(channel);
    state = fs_etpu_gpio_pin_history(channel);

    if( state & 1 )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*=========END OF EXAMPLE OF INITIALIZATION OF eTPU GPIO ===================*/



/*==========================================================================*/
/*                                                                          */
/*          EXAMPLE OF READING THE CPU SPEED OF THE AUTOPILOT               */ 
/*                                                                          */
/*==========================================================================*/
/****************************************************************************/
/*!
@param  none  

@return int32_t    ColdFire CPU speed in Hz

@brief
The function returns the autopilot CPU speed in Hz
*****************************************************************************/
static int32_t getCpuSpeed( void )
{
    int32_t cpu_freq = 0;
    int32_t mfd;
    int32_t rfd;

    mfd = (MP2128UC_FMPLL_SYNCR & MP2128UC_FMPLL_SYNCR_MFD_MASK ) >> 24;
    rfd = (MP2128UC_FMPLL_SYNCR & MP2128UC_FMPLL_SYNCR_RFD_MASK ) >> 19;
    cpu_freq  = (int32_t)((MP2128UC_FSYS_REF * 2 * ( mfd + 2 )) >> rfd);

    return cpu_freq;
} 
/*=========END OF EXAMPLE OF READING THE CPU SPEED OF THE AUTOPILOT=========*/

/*==========================================================================*/
/*                                                                          */
/*              EXAMPLE OF CONFIGURING THE USER-DEFINED INTERRUPTS          */
/*                                                                          */
/*==========================================================================*/

static int32_t enable_usr_irq( uint16_t  irq_controller, uint16_t irq_source,   
                    uint16_t  irq_level,      uint16_t irq_priority,
                    VFNPTR          irq_function )
{
    register uint16_t irq;
    uint32_t           irq_mask;
    int32_t                     irq_mask_register;
    uint16_t          irq_vector;

    irq_controller &= 1; /* isr_controller can be 0 or 1 only */
    irq_vector = ((64<<irq_controller) + irq_source)<<2;

    irq = MP2128UC_INTC_ICRn_IP(irq_priority) + MP2128UC_INTC_ICRn_IL(irq_level);
    *((VOIDFN)( ramBase + irq_vector )) = (VFNPTR)(irq_function);

    irq_mask = 0;
    if( irq_source > 31 )
    {
        irq_mask = 1 << ( irq_source - 31 - 1 );
        irq_mask_register = HIGH;
    }
    else
    {
        irq_mask = 2 << ( irq_source - 1 );
        irq_mask_register = LOW;
    }

    if( irq_controller == 0 )
    {
        MP2128UC_INTC0_ICRn(irq_source) = irq;
        MP2128UC_INTC0_IMRL &= MP2128UC_INTC_ENABLE;
        if( irq_mask_register == HIGH  )
        {
            MP2128UC_INTC0_IMRH &= ~irq_mask; 
        }
        else
        {             
            MP2128UC_INTC0_IMRL &= ~irq_mask;
        }
    }
    else
    {
        MP2128UC_INTC1_ICRn(irq_source) = irq;
        MP2128UC_INTC1_IMRL &= MP2128UC_INTC_ENABLE;
        if( irq_mask_register == HIGH  )
        {
            MP2128UC_INTC1_IMRH &= ~irq_mask; 
        }
        else
        {
            MP2128UC_INTC1_IMRL &= ~irq_mask;
        }
    }  
    return 1;
}
/*======END OF EXAMPLE OF CONFIGURING OF THE USER-DEFINED INTERRUPTS========*/



/*==========================================================================*/
/*                                                                          */
/*              EXAMPLE OF PROGRAMMING RS232 USING eTPU                     */
/*                                                                          */
/*==========================================================================*/
/*  Refer to the "Enhanced Time Processing Unit (eTPU) Reference Manual"
    ETPURM/D by Motorola for more details on configuration options of the eTPU.
*/

/***************************************************************************/
/*!
@param  cpu_frequency [in]  ColdFire CPU frequency

@return none       

@brief
This function sets up eTPU clocking
****************************************************************************/
static void mp2128_get_etpu_freq( int32_t cpu_frequency ) 
{
    etpu_a_tcr1_freq = (uint32_t)( cpu_frequency >> 2 );
    etpu_a_tcr2_freq = (uint32_t)( cpu_frequency >> 4 );  
}

/***************************************************************************/
/*!
@param  channel [in]    eTPU channel to be initialized

@return uint16_t  channel number - if there is an error
                        0              - if initialization was successful
@brief
This function initializes eTPU UART
****************************************************************************/
static uint16_t mp2128_etpu_uart_init( uint16_t tx_channel, uint16_t rx_channel,
                                        unsigned char  priority,   uint32_t  baud_rate,  
                                        unsigned char  bits,       unsigned char  parity,
                                        uint32_t  etpu_freq )
{
    int32_t err_code;
    uint16_t result = 0;
    
    /* PRINT_DEBUG(textQueuePutStr, "Freq %lu, baud %lu", etpu_freq, baud_rate); */

    fs_free_param = *fs_free_param_address;
    err_code = fs_etpu_uart_init (  (unsigned char)tx_channel,
                                    (unsigned char)rx_channel,
                                    priority,
                                    baud_rate,
                                    bits,
                                    parity,
                                    etpu_freq);
    *fs_free_param_address = fs_free_param;

    if (err_code != 0)
    {
        result = tx_channel ;
    }
    else
    {
        result = 0;
        etpu_cie_a = 0;
        etpu_cie_a |= ( 1 << rx_channel );
        etpu_cie_a |= ( 1 << tx_channel );
        eTPU->CIER_A.R |= etpu_cie_a;
    }

    return result;
}

/***************************************************************************/
/*!
@param 

@return

@brief
This function assigns and initializes UART eTPU channels. It is a universal 
function which may be used for initialization of multiple eTPU UARTs.
****************************************************************************/
static uint16_t tpuUartInit(
    uint32_t baud,
    int32_t size,
    int32_t parity, 
    unsigned char txChan,
    uint16_t txLevel,
    uint16_t txPriority,
    unsigned char rxChan,
    uint16_t rxLevel,
    uint16_t rxPriority,
    struct uart_record_t *uart_record,
    VFNPTR pit_rx_function,
    VFNPTR pit_tx_function )
{
    uint32_t    SR_Save;
    uint32_t   *upper_PPA_SCR32_UART_RX;
    uint32_t   *upper_PPA_SCR32_UART_TX;
    uint16_t  *upper_PPA_SCR16_UART_RX_tmp;
    uint16_t  *upper_PPA_SCR16_UART_TX_tmp;

    int32_t            cpu_freq;
    uint16_t  error_code = 0;

    uart_record->txChan = txChan;
    uart_record->rxChan = rxChan;

    disableAll();

    cpu_freq = getCpuSpeed();
    mp2128_get_etpu_freq( cpu_freq );

    error_code = mp2128_etpu_uart_init (
        txChan,
        rxChan,
        FS_ETPU_PRIORITY_HIGH,
        baud>>2,
        size,
        parity,
        etpu_a_tcr2_freq);
    
    if( error_code  == 0 )
    {
        enable_usr_irq( MP2128UC_ETPU_IRQ_CONTROLLER, MP2128UC_IRQ_SOURCE_ETPU_0 + txChan,    
                        txLevel,           txPriority,
                        (VFNPTR)(pit_tx_function));

        upper_PPA_SCR32_UART_TX = &eTPU->CHAN[txChan].SCR.R;
        upper_PPA_SCR16_UART_TX_tmp = ( uint16_t* )upper_PPA_SCR32_UART_TX;
        uart_record->upper_tx = upper_PPA_SCR16_UART_TX_tmp;


        enable_usr_irq( MP2128UC_ETPU_IRQ_CONTROLLER, MP2128UC_IRQ_SOURCE_ETPU_0 + rxChan,    
                        rxLevel,           rxPriority,
                        (VFNPTR)(pit_rx_function));

        upper_PPA_SCR32_UART_RX = &eTPU->CHAN[rxChan].SCR.R;
        upper_PPA_SCR16_UART_RX_tmp = ( uint16_t* )upper_PPA_SCR32_UART_RX;
        uart_record->upper_rx = upper_PPA_SCR16_UART_RX_tmp;
        
        while (eTPU->CHAN[txChan].SCR.B.CIS == 0)
        {
            /* wait for init */
        }

        *upper_PPA_SCR16_UART_RX_tmp = 0xC000;
        *upper_PPA_SCR16_UART_TX_tmp = 0xC000;
   }
   enable();

   return error_code;
}

/***************************************************************************/
/*!
@param      none

@return     int32_t     0 - Tx buffer is NOT empty
                    1 - Tx buffer is empty

@brief
This function checks if the UART Tx buffer is empty.
****************************************************************************/
static int32_t uartTxEmpty( struct uart_record_t *uart_record)
{
    return uart_record->txExtractPtr == uart_record->txInsertPtr;
}

/***************************************************************************/
/*!
@param      character   [in]    the character to output
            tx_channel  [in]    the Tx channel
            upper_tx    [in]    configuration parameter for the Tx channel

@return     1                   - if the character was transmitted/saved to buffer
            0                   - nothing to send

@brief
This is a universal function which puts a character in the eTPU UART port.
This function works with all eTPU channels assigned as eTPU UART output.
****************************************************************************/
static int32_t uartPut( int32_t character, struct uart_record_t *uart_record  )
{
    uint32_t SR_Save; 
    int32_t temp;

    disableAll();
    if( uart_record->txExtractPtr == uart_record->txInsertPtr && ( eTPU->CHAN[uart_record->txChan].SCR.B.DTRS == 1  ) )
    {
        *uart_record->upper_tx = 0xC0C0;
        fs_etpu_uart_write_transmit_data ( uart_record->txChan, character );
        enable();
        return 1;
    }

    temp = (uart_record->txInsertPtr + 1) % UART_BUFFER_SIZE;
    if( temp == uart_record->txExtractPtr)
    {
        enable();
        return 0;
    }
    uart_record->txBuffer[uart_record->txInsertPtr] = character;
    uart_record->txInsertPtr = temp;
    enable();
    return 1;
}

/***************************************************************************/
/*!
@param      none

@return     value received from the Rx buffer

@brief 
This function returns a value from the RX buffer
****************************************************************************/
static int32_t uartGet( struct uart_record_t *uart_record )
{
    uint32_t SR_Save;               /* required for disable and enable */
    uint16_t character;
    
    while( uartStatus(uart_record) == 0 );

    disableAll();
    character = uart_record->rxBuffer[uart_record->rxExtractPtr++]; /* Get character, bump pointer */
    uart_record->rxExtractPtr %= UART_BUFFER_SIZE;			/* Limit the pointer */
    enable();

    return character;
}

/***************************************************************************/
/*!
@param     none

@return    int32_t  0 - UART is not empty
                1 - UART is empty

@brief
Return the status of the receive channel.
****************************************************************************/
static int32_t uartRxEmpty( struct uart_record_t* uart_record )
{
	return (uart_record->rxInsertPtr == uart_record->rxExtractPtr);
}


/***************************************************************************/
/*!
@param 

@return

@brief
The following is a minimal UART receiver interrupt handler. It is uniquely
assigned to the UART_RX from the example. 
****************************************************************************/ 
static void _uartRxISR_internal( struct uart_record_t *uart_record )
{
    uint32_t SR_Save;    
    unsigned char rx_error;
    int32_t     rx_data;
    int32_t     temp;

    disableAll();
    rx_error = 0;

    rx_data = fs_etpu_uart_read_receive_data ( uart_record->rxChan, &rx_error );
    
    temp = (uart_record->rxInsertPtr + 1) % UART_BUFFER_SIZE;
    if (temp != uart_record->rxExtractPtr)
    {
        uart_record->rxBuffer[uart_record->rxInsertPtr] = (uint16_t)rx_data;
        uart_record->rxInsertPtr = temp;
    }
    *uart_record->upper_rx = 0xC0C0;
    enable();
}



void debugUartRxISR_real(void)
{
    _uartRxISR_internal(&_debug_uart_record);
}

void fpgaUartRxISR_real(void)
{
    _uartRxISR_internal(&_fpga_uart_record);
}


/***************************************************************************/
/*!
@param 

@return

@brief
The following is a minimal UART transmitter interrupt handler. It is uniquely
assigned to the UART_TX from the example. 
****************************************************************************/
static void _uartTxISR_internal( struct uart_record_t *uart_record)
{  
    uint32_t SR_Save;

    disableAll();    
    if(uart_record->txExtractPtr != uart_record->txInsertPtr)
    {   
        if( eTPU->CHAN[uart_record->txChan].SCR.B.DTRS == 1 )
        {
		    *uart_record->upper_tx = 0xC0C0;
            fs_etpu_uart_write_transmit_data (
                uart_record->txChan,
                uart_record->txBuffer[uart_record->txExtractPtr]);
            uart_record->txExtractPtr = (uart_record->txExtractPtr+1) % UART_BUFFER_SIZE;            
        }
    }
    else
    {      
        *uart_record->upper_tx = 0xC000;
    }
    enable();
}


void debugUartTxISR_real(void)
{
    _uartTxISR_internal(&_debug_uart_record);
}

void fpgaUartTxISR_real(void)
{
    _uartTxISR_internal(&_fpga_uart_record);
}

/***************************************************************************/
/*!
@param  none

@return 1 - RX buffer has some data
        0 - RX buffer is empty

@brief
This function reports the RX buffer status
****************************************************************************/
static int32_t uartStatus( struct uart_record_t *uart_record )
{
    return (uart_record->rxInsertPtr != uart_record->rxExtractPtr);
}

/*=======END OF EXAMPLE OF PROGRAMMING RS232 USING eTPU ====================*/


/*==========================================================================*/
/*                                                                          */
/*           EXAMPLE OF PROGRAMMING SERVO INPUTS/OUTPUTS                    */
/*                                                                          */
/*==========================================================================*/

/***************************************************************************/
/*!
@param          channel [in] - eTPU channel

@return         0            - if eTPU channel was successfully initialized
                channel      - if initialization has failed

@brief
This function enables an eTPU channel to generate servo pulses 
****************************************************************************/
static uint16_t initServoOut( int32_t channel)
{
    uint32_t SR_Save;
    uint16_t error_code;

    disableAll();
    channel &= 0x1F; 
    ltpu_servo[channel] = ((( MP2128UC_ETPU_QOM_HITIME_DEFAULT * MP2128UC_SERVO_OUT_1MS )/2000L) << 1) & (~1);
    error_code = mp2128_etpu_qom_single_short_init( channel, &ltpu_servo[channel] ); 
    enable();
    
    return error_code;
 }


/***************************************************************************/
/*!
@param  channel     [in]    eTPU channel
        hightime    [in]    duration of the pulse in coarse servo units

@return none

@brief
This function generate a single pulse on channel of duration hightime where 
hightime is in coarse servo units
****************************************************************************/
static void hitServoOut( uint32_t channel, int32_t hightime)
{
    uint32_t SR_Save;


    disableAll();
    if( hightime == 0) 
    {
        enable();
        return;
    }

    if( hightime > MP2128UC_MAX_SERVO_HIGH) 
    {
        hightime = MP2128UC_MAX_SERVO_HIGH;
    }
    if( hightime < MP2128UC_MIN_SERVO_HIGH) 
    {
        hightime = MP2128UC_MIN_SERVO_HIGH;
    }
    channel &= 0x1F; 

    hightime = (hightime*MP2128UC_SERVO_OUT_1MS)/2000;

    ltpu_servo[channel] = (hightime << 1) & (~1);
    mp2128_etpu_qom_single_short_init( channel, &ltpu_servo[channel] ); 
    enable();
}

/***************************************************************************/
/*!
@param  channel [in]    eTPU channel to be initialized

@return channel number  - if there is an error
        0               - if initialization is successful
@brief
This function initializes QOM module of eTPU
****************************************************************************/
static uint16_t mp2128_etpu_qom_single_short_init(  uint16_t channel, int32_t *event_array )
{
    uint32_t SR_Save;
    int32_t err_code;
    uint16_t result;
    
    disableAll();
    fs_free_param = *fs_free_param_address;
    err_code = fs_etpu_qom_init (   (unsigned char)channel,
                                    MP2128UC_ETPU_QOM_PRIORITY,      
                                    MP2128UC_ETPU_QOM_MODE,	       
                                    MP2128UC_ETPU_QOM_TIME_BASE,	   
                                    MP2128UC_ETPU_QOM_INIT_PIN,	   
                                    MP2128UC_ETPU_QOM_FIRST_MATCH,
                                    (uint32_t*)0,	                        
                                    0,	                        
                                    MP2128UC_ETPU_QOM_EVENT_SIZE,	
                                    ( union etpu_events_array *)event_array);
    *fs_free_param_address = fs_free_param; 

    if (err_code != 0)
    {
        result =  channel;
    }
    else
    {
        result = 0;
    }

    enable();
    return result;
}


/*==========================================================================*/
/*                                                                          */
/*          EXAMPLE OF READING SERVO SIGNALS                                */ 
/*                                                                          */
/*==========================================================================*/
/*
 * Rx servos use the PTA (Programmable Time Accumulate) function of the eTPU.
 *
 * WARNING: strictly speaking, this routine should really generate an interrupt
 *          when the count has completed as it does not operate in "one-shot" mode. 
 */

/***************************************************************************/
/*!
@param      channel [in]    eTPU channel to be read

@return     none

@brief
This function reads servo pulses - each TPU channel requires a separate ISR.
The read value will be stored in the global array.
****************************************************************************/
static void readTPU_PTA( int32_t channel)
{
    uint32_t  res;          

    fs_etpu_ppa_get_accumulation ( channel, &res ); 
    tpuResult[ channel ] += res;
    tpuCount[ channel ]++;   
}


/***************************************************************************/
/*!
@param  none

@return none

@brief
This function handles interrupts from eTPU channels.
****************************************************************************/
static int32_t ttt = 5000;

void servoRxISR1_real(void)
{
    uint32_t SR_Save;
    int32_t i;


    disableAll();
    readTPU_PTA( MP2128UC_ETPU_PPA_SERVO_IN_1_CHAN );
    for(i=0; i < ttt; i++);
    *upper_PPA_SCR16_RX_SERVO1 = 0xC0C0;
    enable();
}



/***************************************************************************/
/*!
@param  none

@return none

@brief
This function handles interrupts from eTPU channels.
****************************************************************************/
__attribute__((__interrupt_handler__)) static void TPUreadRC_ISR2( void)
{
    uint32_t SR_Save;

    disableAll();
    readTPU_PTA( MP2128UC_ETPU_PPA_SERVO_IN_2_CHAN );
    *upper_PPA_SCR16_RX_SERVO2 = 0xC0C0;
    enable();

}



/****************************************************************************/
/*!
@param  channel [in]        eTPU channel
        *upper_ppa[in/out]  address of the configuration register
        irq_function        IRQ function associated with the eTPU channel

@return error code          - equals to channel number if initialization wasn't successful
                            - equals to 0 if initialization was successful

@brief
This function initializes eTPU channel to read pulse duration
****************************************************************************/
static uint16_t initRxServo( uint32_t channel, uint32_t *upper_ppa, VFNPTR irq_function )
{
    uint32_t SR_Save;
    volatile uint16_t* upper_PPA_SCR16_RX_SERVO;
    uint16_t error_code;

    disableAll();
    tpuCount[channel]=0;
    tpuResult[channel]=0;

    error_code = mp2128_etpu_ppa_init( channel );

    if( error_code == 0 )
    {
        enable_usr_irq( MP2128UC_ETPU_IRQ_CONTROLLER,   MP2128UC_IRQ_SOURCE_ETPU_0 + channel, 
                        MP2128UC_SERVO_RX_LEVEL,             MP2128UC_SERVO_RX_PRIORITY, 
                        (VFNPTR)(irq_function) );


        upper_PPA_SCR16_RX_SERVO = (volatile uint16_t *)&eTPU->CHAN[channel].SCR.R;         
        *upper_PPA_SCR16_RX_SERVO = 0xC0C0;
        *upper_ppa = upper_PPA_SCR16_RX_SERVO;
    }    
    enable();
    return error_code;
 }

/***************************************************************************/
/*!
@param      channel [in]    eTPU channel to be read

@return     duration of the pulse in coarse servo units 

@brief
This function returns the total high time for a servo input averaged over
1 cycle. If the servo signal is proper, returns a value between 
2000 (1 ms pulse width) and 4000 (2 ms pulse width)          
****************************************************************************/
static int32_t readRxServo( uint32_t channel )
{
    int32_t tpresult;
    uint32_t SR_Save; /* required for disable and enable */

    disableAll();
    /*   if( tpuCount[channel] == 0) tpuCount[channel] = 1; */
    if( tpuCount[channel] == 0) 
    {         
        enable();
        return 0;
    }
    tpresult = tpuResult[channel]/tpuCount[channel];

    tpuResult[channel]=0;
    tpuCount [channel]=0;   
    
    tpresult = (tpresult*2000L)/MP2128UC_RC_RX_1MS;
    enable();
    return (tpresult);			/* Return the count value */
}

/***************************************************************************/
/*!
@param      channel [in]    eTPU channel to be initialized

@return     error code      - equals to channel number if there is an error
                            - equals to 0 if initialization was successful
@brief
This function initializes PPA module of eTPU 
****************************************************************************/
static uint16_t mp2128_etpu_ppa_init( uint32_t channel )
{
    uint32_t SR_Save;    
    uint32_t max_counts;
    int32_t err_code;
    uint16_t result = 0;

    max_counts = MP2128UC_ETPU_PPA_RC_MAX_COUNT;
    
    fs_free_param = *fs_free_param_address;
    err_code = fs_etpu_ppa_init(    (unsigned char) channel,	           
                                    MP2128UC_ETPU_PPA_PRIORIY,	    /* priority: High */
                                    MP2128UC_ETPU_PPA_TIME_BASE,    /* timebase: TCR2 */
                                    MP2128UC_ETPU_PPA_MODE,	        /* mode: FS_ETPU_PPA_HIGH_PULSE */
                                                                    //FS_ETPU_PPA_RISING_EDGE,
                                    max_counts,	                    /* max_count: 10 */
                                    MP2128UC_ETPU_PPA_SAMPLE_TIME);	/* sample_time: 100000 */
    *fs_free_param_address = fs_free_param;
    

    if (err_code != 0)
    {
        result = channel;
    }
    else
    {    
        result = 0;
        etpu_cie_a |= ( 1 << channel );
        eTPU->CIER_A.R |= etpu_cie_a;
    } 

    return result;
}




/***************************************************************************
 *    EXPORTED FUNCTIONS                                                   *
 ***************************************************************************/

/***************************************************************************/
/*!
@param      function    [in]    pointer to the function to allow retrieval
                                of field IDs            

@return     0

@brief
This function initializes communication with the autopilot to allow the user 
code to function
****************************************************************************/
int32_t mpUserInit( int32_t (*function)( void **result, int32_t id)) 
{
   saveGetVarPointer = function;

   return 0;  
}

static int _fpga_receive(unsigned char* cp)
{
    if( uartRxEmpty(&_fpga_uart_record) ) {
        return 0;
    }
    else {
        *cp = uartGet(&_fpga_uart_record);
        return 1;
    }
}

static int _fpga_send(unsigned char c)
{
    if( uartTxEmpty(&_fpga_uart_record) ) {
        uartPut( (int32_t) c, &_fpga_uart_record);
        return 1;
    }
    else {
        return 0;
    }
}

#if 0
static struct fpga_io_handler_t _fpga_io_handler = {
    .receiver = _fpga_receive,
    .sender   = _fpga_send
};
#endif

static int _debug_receive(unsigned char* cp)
{
    if( uartRxEmpty(&_debug_uart_record) ) {
        return 0;
    }
    else {
        *cp = uartGet(&_debug_uart_record);
        return 1;
    }
}

static int _debug_send(unsigned char c)
{
    if( uartTxEmpty(&_debug_uart_record) ) {
        uartPut( (int32_t) c, &_debug_uart_record);
        return 1;
    }
    else {
        return 0;
    }
}

/***************************************************************************/
/*!
@param      result  [in/out]    replacement value 
            userEventIdentifier [in]    event ID

@return     USER_RETURN_IGNORE  - Discard the result but keep calling the function
            USER_RETURN_REPLACE - Use the result if applicable
            USER_RETURN_UNUSED  - Discard the result and doesn't call the function
                                  again

@brief
This function is called during initialization to determine that any event is used.
If an event is used, then this event will be called by the autopilot at different
rates (1Hz, 5Hz, 30Hz, one-time only) depending on the property of the event. 
****************************************************************************/

#define ENABLE_SERVO_RX 1
#define ENABLE_SERVO_TX 1
#define ENABLE_GPIO     1

int32_t mpUserEvent( int32_t *result, int32_t userEventIdentifier)
{
	int32_t ok;
    static int32_t *locationEast;
    static int32_t *locationNorth;
    static int32_t *hardwareDisable;
    static int32_t gpioTestConfError = 0;
    static int32_t gpioProbeConfError = 0;
    static int32_t fpgaUartConfError = 0;
    static int32_t debugUartConfError = 0;
    static int32_t servoRxConfError = 0;
    static int32_t servoTxConfError = 0;
    static int32_t configurationError = 0;
    static int32_t *free_etpu_address;
	
    static int32_t hightime = SERVO_OUT_1_5MS;
    static int32_t *scratchField1 = NULL;
    static int32_t *scratchField2 = NULL;

    char        lastReceivedChar = ' ';
    int32_t        servo_read_result = 0;
    
    /* TBD: This needs to be centralized. */
    typedef enum {
        k_debug_text_field_id = MPFID_SCRATCH_1
    } micropilot_field_id_t;
    
    static int32_t *debug_text_field = NULL;
    static int32_t *gUserField1 = NULL;

    switch( userEventIdentifier)
    {
        case USER_HARDWARE_INITIALIZED : // called only once 

            ok = getMPVarPointer( (void **)&locationEast,  1012);
            ok = getMPVarPointer( (void **)&locationNorth, 1013);
            ok = getMPVarPointer( (void **)&hardwareDisable, 594);
            
            /* This address is required for eTPU to function */
            ok = getMPVarPointer( (void **)&free_etpu_address, 1525 );
            fs_free_param_address = (*(uint32_t*)(*(uint32_t*)(&free_etpu_address)));

            /*! When using eTPU channels, make sure that they are not used by the autopilot !*/

            //Open TPU pin number MP2128_TEST_ETPU_GPIO as a digital output and get a pointer
            //to the 1st scratch field, which is used to control the digital output
#ifdef ENABLE_GPIO
            gpioTestConfError = etpuGpioInit( MP2128_ETPU_GPIO_OUTPUT_CHAN, FS_ETPU_PRIORITY_HIGH );
#endif
            
            //Open TPU pin number MP2128_PROBE_ETPU_GPIO as a digital input and get a pointer
            //to the 2nd scratch field, which is set to the value of the digital input
#ifdef ENABLE_GPIO
            gpioProbeConfError = etpuGpioInit( MP2128_ETPU_GPIO_INPUT_CHAN, FS_ETPU_PRIORITY_HIGH );
#endif
            // getMPVarPointer((void**)&scratchField2, 1546);

            fpgaUartConfError = tpuUartInit(
                MP2128UC_FPGA_UART_BAUD,
                MP2128UC_FPGA_UART_SIZE,
                MP2128UC_FPGA_UART_PARITY,
                MP2128UC_FPGA_UART_TX_CHAN,
                MP2128UC_FPGA_UART_TX_LEVEL,
                MP2128UC_FPGA_UART_TX_PRIORITY,
                MP2128UC_FPGA_UART_RX_CHAN,
                MP2128UC_FPGA_UART_RX_LEVEL,
                MP2128UC_FPGA_UART_RX_PRIORITY,
                &_fpga_uart_record,
                fpgaUartRxISR,
                fpgaUartTxISR );

            debugUartConfError = tpuUartInit(
                MP2128UC_DEBUG_UART_BAUD,
                MP2128UC_DEBUG_UART_SIZE,
                MP2128UC_DEBUG_UART_PARITY,
                MP2128UC_DEBUG_UART_TX_CHAN,
                MP2128UC_DEBUG_UART_TX_LEVEL,
                MP2128UC_DEBUG_UART_TX_PRIORITY,
                MP2128UC_DEBUG_UART_RX_CHAN,
                MP2128UC_DEBUG_UART_RX_LEVEL,
                MP2128UC_DEBUG_UART_RX_PRIORITY,
                &_debug_uart_record,
                debugUartRxISR,
                debugUartTxISR );
            
#ifdef ENABLE_SERVO_TX
            servoTxConfError =  initServoOut(   MP2128UC_ETPU_SERVO_TEST1_CHAN );
#endif
#ifdef ENABLE_SERVO_RX
            servoRxConfError =  initRxServo (   MP2128UC_ETPU_PPA_SERVO_IN_1_CHAN,
                                                &upper_PPA_SCR16_RX_SERVO1,
                                                servoRxISR1 );
#endif

            if( (gpioTestConfError  != 0)   || 
                (gpioProbeConfError != 0)   || 
                (fpgaUartConfError  != 0)   ||
                (debugUartConfError != 0)   ||
                (gpioTestConfError  != 0)   ||
                (servoTxConfError   != 0)   ||
                (servoRxConfError   != 0)   )
            {
                configurationError = 1;    
            }
            
            /* If this doesn't work, we'll have no way to print debug output.  So,
             * there isn't much point in checking for an error. */
            ok = getMPVarPointer((void**)  &debug_text_field,          k_debug_text_field_id);
            
            initialize_telemetry_tables(&_text_queue_dbg);

            return USER_RETURN_IGNORE; /* don't do anything with the results */

        case USER_30HZ_LOOP :            
            /* If there is a single error, then everything doesn't work.        */
            /* As an option, the user can partially use the available devices.  */
            if( configurationError == 0 )
            {
				{
#ifdef ENABLE_GPIO
					static int foo = 0;
					if(1 == foo) {
						fs_etpu_gpio_output_high ( MP2128_ETPU_GPIO_OUTPUT_CHAN );
					}
					else {
						fs_etpu_gpio_output_low ( MP2128_ETPU_GPIO_OUTPUT_CHAN );
					}
					foo = 1 - foo;
#endif
#if 1
                    if(0 == (g30hzCount % 30)) {
                        //PRINT_DEBUG(textQueuePutStr, "Cycle %d", g30hzCount);
                    }
#endif
                    *gUserField1 = g30hzCount;
				}

                //Read the state of the digital input and put its value into a field
#ifdef ENABLE_GPIO
                *scratchField2 = mp2128_etpu_gpio_read( MP2128_ETPU_GPIO_INPUT_CHAN );
#endif
#if 0
                /* Put debug output into the Uart */
                if( uartTxEmpty(&_debug_uart_record) ) {
                    while(true) {
                        char char_buf;
                        unsigned charsFetched = 0;
                        textQueueGet(&char_buf, &charsFetched);
                        if(0 == charsFetched) {
                            break;
                        }
                        uartPut( (int32_t) char_buf, &_fpga_uart_record);
                    }
                }
                uartPut( (int32_t) 'X', &_fpga_uart_record);
                uartPut( (int32_t) 'Y', &_debug_uart_record);
                //uartPut( 0x55, &_debug_uart_record);
                //uartPut( 0xFF, &_debug_uart_record);
                //uartPut( 0x55, &_debug_uart_record);
                //uartPut( 0x55, &_debug_uart_record);
                //uartPut( 0x00, &_debug_uart_record);
                //uartPut( 0x07, &_debug_uart_record);
                //uartPut( 0x00, &_debug_uart_record);
                //uartPut( 0x0F, &_debug_uart_record);
#endif

                //run_fpga_interface(&_fpga_io_handler);
                
                /* Read the last received character from the Rx buffer */
#if 1
                if( !uartRxEmpty(&_debug_uart_record) )
                {
                    lastReceivedChar = uartGet(&_debug_uart_record);
                    PRINT_DEBUG_I(&_text_queue_dbg, (int) lastReceivedChar);
                }
#endif
#ifdef ENABLE_SERVO_RX
                /* read eTPU RX servo channel */
                servo_read_result = readRxServo(MP2128UC_ETPU_PPA_SERVO_IN_1_CHAN);
#endif
#ifdef ENABLE_SERVO_TX
                /* send servo signal */
                hitServoOut( MP2128UC_ETPU_SERVO_TEST1_CHAN, hightime );
#endif
            }
			else if(scratchField1 != 0)
			{
				*scratchField1 = 999;
			}
            
            {
                static unsigned int frob = 0;
                if(frob++ > 150) {
                    frob = 0;
                    PRINT_DEBUG_S(&_text_queue_dbg, "Hey");
                }
            }
            
            _put_debug_text_in_telemetry(debug_text_field);
 
            g30hzCount++;
            return USER_RETURN_IGNORE;

        default:
            return USER_RETURN_UNUSED;   /* This particular user event is not handled so don't call it again*/
    }
}
