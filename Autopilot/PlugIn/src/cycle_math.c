#include "cycle_math.h"

#ifdef __m68k__ /* mp2128 cross-compiler. */
static const uint32_t UINT32_MAX = 0xFFFFFFFFUL;
#elif defined __CYGWIN__ /* Cygwin native compiler. */
#include <stdint.h>
#else
#error Unknown compiler!  You need to provide the definition of UINT32_MAX.
#endif

uint32_t cycle_difference(const uint32_t later, const uint32_t earlier)
{
    return (later >= earlier)
        ? later - earlier
        : later + (UINT32_MAX - earlier) + 1;
}
