#include "text_queue.h"

static char _fifo[kTextQueueCapacity];
static unsigned firstOccupiedSlot = 0;
static unsigned fifoSize = 0;

void textQueuePutChar(const char c)
{
    const unsigned nextFreeSlot = (firstOccupiedSlot + fifoSize) % kTextQueueCapacity;
    if(fifoSize >= kTextQueueCapacity) {
        /* Do nothing, we've already signaled a problem. */
    }
    else if(fifoSize == kTextQueueCapacity - 1) {
        _fifo[nextFreeSlot] = '*'; /* Overflow. */
        fifoSize++;
    }
    else {
        _fifo[nextFreeSlot] = c;
        fifoSize++;
    }
}

void textQueuePutStr(const char* str, const unsigned len)
{
    unsigned i;
    for(i = 0; i < len; ++i) {
        textQueuePutChar(str[i]);
    }
}

void textQueueGet(char* charBuffer, unsigned* const charsFetched)
{
    if(0 == fifoSize) {
        *charsFetched = 0;
    }
    else {
        *charBuffer = _fifo[firstOccupiedSlot];
        *charsFetched = 1;
        firstOccupiedSlot = (firstOccupiedSlot + 1) % kTextQueueCapacity;
        fifoSize--;
    }
}
