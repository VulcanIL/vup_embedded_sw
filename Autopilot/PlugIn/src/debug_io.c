#include "debug_io.h"

/**
 * This implementation was copped from the following website:
 * http://www.strudel.org.uk/itoa/
 * Specifically it is a slightly modified version of Lukas Chmela's implementation, v0.4.
 */
void itoa10(int value, char* result)
{
    const int base = 10;

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    /* Apply negative sign. */
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
}

void print_debug_str(const struct debug_interface_t* dbg, const char* str, const unsigned len)
{
    dbg->send_text(str, len);
}

void print_debug_int(const struct debug_interface_t* dbg, const int value)
{
    enum { kBufferSize = sizeof(value) * 8 + 1 };
    char buffer[kBufferSize];
    itoa10(value, buffer);
    dbg->send_text(buffer, strlen(buffer));
}
