#include "fpga_command_queue.h"

static struct fpga_queue_entry_t _commandQueue[kFPGACommandQueueCapacity];
static unsigned _first_occupied_slot = 0;
static unsigned _fifo_size = 0;

int fpga_command_queue_put(const struct fpga_queue_entry_t entry)
{
    const unsigned nextFreeSlot = (_first_occupied_slot + _fifo_size) % kFPGACommandQueueCapacity;
    if(_fifo_size >= kFPGACommandQueueCapacity) {
        return -1; /* Overflow! */
    }
    else {
        _commandQueue[nextFreeSlot] = entry;
        _fifo_size++;
        return 0;
    }
}

int fpga_command_queue_get(struct fpga_queue_entry_t* const entry, bool* const gotEntry)
{
    if(0 == _fifo_size) {
        *gotEntry = false;
        return 0;
    }
    else {
        *entry = _commandQueue[_first_occupied_slot];
        *gotEntry = true;
        _first_occupied_slot = (_first_occupied_slot + 1) % kFPGACommandQueueCapacity;
        _fifo_size--;
        return 0;
    }
}

void fpga_command_queue_clear()
{
    _first_occupied_slot = 0;
    _fifo_size = 0;
}

unsigned fpga_command_queue_size()
{
    return _fifo_size;
}
