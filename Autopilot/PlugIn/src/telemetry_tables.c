#include <string.h>
#include "telemetry_tables.h"
#include "debug_io.h"
#include "mpsdk/src/mp2128/include/mp2128.h"
#include "mpsdk/include/mpfields.h"

enum {
    kSlotsPerTelemetryFrame = 20,
    kTelemetryFrameCount = 5,
    
    kDebugTextFieldId = MPFID_SCRATCH_1
};

static const struct {
    struct {
        uint32_t slots[kSlotsPerTelemetryFrame];
    } frames[kTelemetryFrameCount];
} _telemetry_table = {
    .frames = {
        [0] = {
            .slots = {
                [ 0] = kDebugTextFieldId,
                [ 1] = 0,
                [ 2] = 0,
                [ 3] = 0,
                [ 4] = 0,
                [ 5] = 0,
                [ 6] = 0,
                [ 7] = 0,
                [ 8] = 0,
                [ 9] = 0,
                [10] = 0,
                [11] = 0,
                [12] = 0,
                [13] = 0,
                [14] = 0,
                [15] = 0,
                [16] = 0,
                [17] = 0,
                [18] = 0,
                [19] = 0,
            }
        },
        [1] = {
            .slots = {
                [ 0] = kDebugTextFieldId,
                [ 1] = 0,
                [ 2] = 0,
                [ 3] = 0,
                [ 4] = 0,
                [ 5] = 0,
                [ 6] = 0,
                [ 7] = 0,
                [ 8] = 0,
                [ 9] = 0,
                [10] = 0,
                [11] = 0,
                [12] = 0,
                [13] = 0,
                [14] = 0,
                [15] = 0,
                [16] = 0,
                [17] = 0,
                [18] = 0,
                [19] = 0,
            }
        },
        [2] = {
            .slots = {
                [ 0] = kDebugTextFieldId,
                [ 1] = 0,
                [ 2] = 0,
                [ 3] = 0,
                [ 4] = 0,
                [ 5] = 0,
                [ 6] = 0,
                [ 7] = 0,
                [ 8] = 0,
                [ 9] = 0,
                [10] = 0,
                [11] = 0,
                [12] = 0,
                [13] = 0,
                [14] = 0,
                [15] = 0,
                [16] = 0,
                [17] = 0,
                [18] = 0,
                [19] = 0,
            }
        },
        [3] = {
            .slots = {
                [ 0] = kDebugTextFieldId,
                [ 1] = 0,
                [ 2] = 0,
                [ 3] = 0,
                [ 4] = 0,
                [ 5] = 0,
                [ 6] = 0,
                [ 7] = 0,
                [ 8] = 0,
                [ 9] = 0,
                [10] = 0,
                [11] = 0,
                [12] = 0,
                [13] = 0,
                [14] = 0,
                [15] = 0,
                [16] = 0,
                [17] = 0,
                [18] = 0,
                [19] = 0,
            }
        },
        [4] = {
            .slots = {
                [ 0] = kDebugTextFieldId,
                [ 1] = 0,
                [ 2] = 0,
                [ 3] = 0,
                [ 4] = 0,
                [ 5] = 0,
                [ 6] = 0,
                [ 7] = 0,
                [ 8] = 0,
                [ 9] = 0,
                [10] = 0,
                [11] = 0,
                [12] = 0,
                [13] = 0,
                [14] = 0,
                [15] = 0,
                [16] = 0,
                [17] = 0,
                [18] = 0,
                [19] = 0,
            }
        },
    }
};

/**
 * @brief 
 * @param result
 * @param id
 * @return 
 * @todo This needs to be globalized properly.
 */
extern int32_t getMPVarPointer( void **result, int32_t id);

void initialize_telemetry_tables(const struct debug_interface_t *dbg)
{
    unsigned i, j;
    int32_t *enable_user_telemetry_field = NULL;
    int32_t outcome = 0;
    enum { kSuccess = 0 };
    
    for(i = 0; i < kTelemetryFrameCount; i++) {
        for(j = 0; j < kSlotsPerTelemetryFrame; j++) {
            
            const uint32_t this_slot_id    = MPFID_TELEMETRY_0 + (i * kSlotsPerTelemetryFrame) + j;
            const uint32_t id_in_this_slot = _telemetry_table.frames[i].slots[j];
            int32_t *pointer_to_this_slot  = NULL;
            
            outcome = getMPVarPointer((void **) &pointer_to_this_slot, this_slot_id);
            
            if(kSuccess == outcome) {
                *pointer_to_this_slot = id_in_this_slot;
            }
            else {
                PRINT_DEBUG_I(dbg, outcome);
                return; /* This is bad, no point in continuing. */
            }
        }
    }
    
    outcome = getMPVarPointer((void **) &enable_user_telemetry_field, MPFID_ENABLE_TELEMETRY);
    if(kSuccess == outcome) {
        *enable_user_telemetry_field = 1;
    }
    else {
        PRINT_DEBUG_I(dbg, outcome);
    }
}
