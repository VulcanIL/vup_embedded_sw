
/**
 * An example program showing how to connect to Horizon in order to e.g. read telemetry from
 * a UAV.  In order for this to work, you need to do several things:
 *
 *   1. You need to run the executable from the Horizon installation directory, to wit,
 *      C:\Program Files (x86)\MicroPilot\Horizon3.7.  The executable doesn't have to be
 *      in that directory, but you do.  Being in a different directory and tweaking your
 *      PATH doesn't work.
 *   2. You need to call mpInitLink with an empty string for the port parameter, which will
 *      cause it to look at the uav#.ini file for its connection properties.  Passing the
 *      address of the localhost for this parameter doesn't work, even though it's advertised
 *      to in the Doxygen documentation.
 *   3. You need to create a custom uav#.ini file (just make a copy of uav1.ini).  You can't
 *      use uav1.ini for two reasons: (i) Horizon is already using it, and (2) you need to
 *      make the following changes to it (documented on page 105 of the XTENDER Programmer's
 *      Guide):
 *          SERIAL=127.0.0.1
 *          TCPIP-PORT=7501
 *          clientPortType=-1
 *   4. You need to run the code as Administrator.
 *
 * This program reads and decodes the debug I/O portion of the telemetry frame, allowing
 * error text output from the autopilot to be displayed on a console.
 */

#if _LINUX || __APPLE__
#include "linux.h"
#else
#include <windows.h>
#endif

// For now we'll hard-code single UAV.

//#if SINGLE_UAV
#include "c-single.h"
//#else
//#include "c-multi.h"
//#endif

#include "mpconsts.h"
#include "mperrors.h"
#include "mpTypedefs.h"
#include "mpgcstelemetry.h"

#if _LINUX || __APPLE__
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#endif

#include <iostream>
#include <chrono>
#include <thread>
#include <functional>

using namespace std;

namespace {

	bool _timeToQuit = false;

	class PlaneCleanup {
	public:
		PlaneCleanup(const unsigned planeId) : _planeId(planeId)
		{
		}
		~PlaneCleanup()
		{
			const int32_t status = mpDelete(_planeId);
			if (MP_OK != status) {
				cerr << "Failed to delete aircraft: " << status << endl;
			}
			// cout << "Deleted aircraft." << endl;
		}
	private:
		const unsigned _planeId;
	};

	class LinkCleanup {
	public:
		LinkCleanup(const unsigned planeId) : _planeId(planeId)
		{
		}
		~LinkCleanup()
		{
			const int32_t status = mpCloseLink(_planeId);
			if (MP_OK != status) {
				cerr << "Failed to close link: " << status << endl;
			}
			// cout << "Closed link." << endl;
		}
	private:
		const unsigned _planeId;
	};

	void _unpackValue(const uint32_t value)
	{
		static int lastSequence = -1;
		const int sequence = 0xFF & (value >> 24);
		if (sequence == lastSequence) {
			return; // No new data since last time.
		}
		if ((lastSequence >= 0) && (sequence != ((lastSequence + 1) % 256))) {
			cout << endl << "... (expected " << static_cast<unsigned>(lastSequence + 1) <<
				", got " << static_cast<unsigned>(sequence) << ")" << endl;
		}
		for (unsigned i = 1; i < 4; ++i) {
			const unsigned shift = 24 - (8 * i);
			const char c = static_cast<char>(0xFF & (value >> shift));
			if ('\0' != c) {
				cout << c << flush;
			}
		}
		lastSequence = sequence;
	}

	// This was copped from:
	// https://docs.microsoft.com/en-us/windows/console/registering-a-control-handler-function
	BOOL WINAPI _controlHandler(_In_ DWORD controlType)
	{
		switch (controlType)
		{
		case CTRL_C_EVENT:
		case CTRL_CLOSE_EVENT:
		case CTRL_BREAK_EVENT:
		case CTRL_LOGOFF_EVENT:
		case CTRL_SHUTDOWN_EVENT:
			cout << "Exiting." << endl;
			_timeToQuit = true;
			return TRUE;
		default:
			return FALSE; // Ignore everything else.
		}
	}
}

int main(int argc, char** argv)
{
	if (!SetConsoleCtrlHandler(static_cast<PHANDLER_ROUTINE>(_controlHandler), TRUE)) {
		cerr << "Failed to register ctrl-C handler." << endl;
		return EXIT_FAILURE;
	}

	enum { kPlaneId = 6 };
	int32_t status;

	// cout << "Creating aircraft..." << endl;
	status = mpCreate(kPlaneId);
	PlaneCleanup planeCleanup(kPlaneId); // Need to delete the plane even if creating it failed.
	if (MP_OK != status) {
		cerr << "Failed to create aircraft: " << status << endl;
		return EXIT_FAILURE;
	}

	// cout << "Connecting to HORIZON..." << endl;
	char localhost[] = ""; // Read from .ini file.
	status = mpInitLink(kPlaneId, localhost);
	LinkCleanup linkCleanup(kPlaneId);
	if (MP_OK != status) {
		cerr << "Failed to create link: " << status << endl;
		return EXIT_FAILURE;
	}

	// This was copped from
	// https://stackoverflow.com/questions/21057676/need-to-call-a-function-at-periodic-time-intervals-in-c
	enum { kIntervalMillis = 100 }; // Poll at twice the update rate of 5/second.
	while(! _timeToQuit) {
		// cout << "Reading a variable..." << endl;
		int32_t value = 0;
		status = mpReadVar(kPlaneId, 1545, &value, AUTO_MODE, NORMAL_MODE);
		if (MP_OK == status) {
			_unpackValue(value);
		}
		else {
			const char* line = "-------------";
			cerr << endl << line << endl << "Failed to read variable value: " << status << endl << line << endl;
		}
		auto wakeTime = std::chrono::steady_clock::now() + std::chrono::milliseconds(kIntervalMillis);
		std::this_thread::sleep_until(wakeTime);
	}


	return EXIT_SUCCESS;
}
